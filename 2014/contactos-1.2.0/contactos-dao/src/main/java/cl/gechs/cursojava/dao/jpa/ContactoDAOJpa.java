package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.dao.ContactoDAO;
import cl.gechs.cursojava.model.Contacto;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Stateful
@javax.ejb.Local(ContactoDAO.class)
public class ContactoDAOJpa extends BaseDAOJpa<Contacto> implements ContactoDAO {

    private static final Logger logger = Logger.getLogger(ContactoDAOJpa.class);
    @Produces
    @PersistenceContext
    private static EntityManager em;

    @Override
    public Class<Contacto> getObjectClass() {
        return Contacto.class;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Contacto> getContactos() {
        Query query = this.getEntityManager().createQuery("FROM Contacto");
        try {
            return (List<Contacto>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public Contacto getContacto(long idContacto) {
        try {
            return this.getById(idContacto);
        } catch (Exception ex) {
            logger.error("Error al recuperar Usuarios.", ex);
            return null;
        }
    }
}
