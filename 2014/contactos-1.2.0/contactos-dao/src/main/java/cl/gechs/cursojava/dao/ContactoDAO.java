package cl.gechs.cursojava.dao;

import java.util.List;
import cl.gechs.cursojava.model.Contacto;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Local
public interface ContactoDAO extends BaseDAO<Contacto> {

    public List<Contacto> getContactos();

    public Contacto getContacto(long idContacto);
}
