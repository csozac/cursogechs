package cl.gechs.cursojava;

import cl.gechs.cursojava.dao.ContactoDAO;
import cl.gechs.cursojava.dao.jpa.ContactoDAOJpa;
import cl.gechs.cursojava.model.Contacto;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail dot>
 */
@RunWith(Arquillian.class)
public class ContactoDAOTest {

    @EJB
    private ContactoDAO userDAO;

    @Deployment(name = "contactos-dao", order = 1, testable = true)
    public static WebArchive createDatabaseDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class, "contactos-dao.war")
                .addPackage(ContactoDAO.class.getPackage())
                .addPackage(ContactoDAOJpa.class.getPackage())
                .addPackage(Contacto.class.getPackage())
                .addAsResource("persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
        System.out.println(war.toString(true));
        return war;
    }

    @Test
    @OperateOnDeployment("contactos-dao")
    @InSequence(1)
    public void testUserDAOAddUpdate() {
        Assert.assertNotNull(this.userDAO);

        Contacto contacto = new Contacto();
        contacto.setNombre("Carlos Soza");
        contacto.setEmail("carlos.soza@gmail.com");
        contacto.setDireccion("Santiago Chile");
        contacto.setTelefono("+5633333");

        //insert
        Assert.assertTrue(this.userDAO.save(contacto));

        //update
        contacto.setTelefono("+562223332");
        Assert.assertTrue(this.userDAO.update(contacto));

        //delete
        Assert.assertTrue(this.userDAO.delete(contacto));
    }
}
