package cl.gechs.cursojava.contactos.restservices;

import cl.gechs.cursojava.model.Contacto;
import cl.gechs.cursojava.service.ContactoServiceLocal;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Path("/contacto")
public class ContactoREST {

    private static final Logger logger = Logger.getLogger(ContactoREST.class);
    private static ContactoServiceLocal contactoService;

    static {
        contactoService = (ContactoServiceLocal) BaseREST.resourceLookup("java:global/contactos-service/ContactoService!cl.gechs.cursojava.service.ContactoServiceLocal");
    }

    @GET
    @Path("/contactosList")
    @Produces("application/json")
    public Response obtenerUsuarios() {
        JSONObject output = new JSONObject();
        List<Contacto> users = contactoService.getContactos();
        JSONArray userArray = new JSONArray();
        for (Contacto u : users) {
            JSONObject uj = new JSONObject();
            uj.put("nombre", u.getNombre());
            uj.put("email", u.getEmail());
            uj.put("direccion", u.getDireccion());
            uj.put("telefono", u.getTelefono());
            userArray.add(uj);
        }
        output.put("contacto_list", userArray);
        return Response.ok(output.toString()).build();
    }

    @GET
    @Path("/obtener/{contactoId}")
    @Produces("application/json")
    public Response obtenerUsuario(@PathParam("contactoId") Long contactoId) {
        JSONObject output = new JSONObject();
        Contacto u = contactoService.getContacto(contactoId);
        JSONObject uj = new JSONObject();
        uj.put("nombre", u.getNombre());
        uj.put("email", u.getEmail());
        uj.put("direccion", u.getDireccion());
        uj.put("telefono", u.getTelefono());
        output.put("contacto", uj);
        return Response.ok(output.toString()).build();
    }
}
