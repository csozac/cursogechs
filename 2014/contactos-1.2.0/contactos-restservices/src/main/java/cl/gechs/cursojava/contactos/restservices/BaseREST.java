package cl.gechs.cursojava.contactos.restservices;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail.com>
 */
public class BaseREST {

    public static Object resourceLookup(String jndiName) {
        try {
            final Hashtable jndiProperties = new Hashtable();
            jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
            final Context context = new InitialContext(jndiProperties);

            return context.lookup(jndiName);
        } catch (Exception ex) {
            return null;
        }
    }
}