package cl.gechs.cursojava.contactos.servlets;

import cl.gechs.cursojava.model.Contacto;
import cl.gechs.cursojava.service.ContactoServiceLocal;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@WebServlet("/servlets/contactos/ContactoListServlet")
public class ContactoListServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(ContactoListServlet.class);
    @EJB(mappedName = "java:global/contactos-service/ContactoService!cl.gechs.cursojava.service.ContactoServiceLocal")
    private static ContactoServiceLocal userService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        JSONObject output = new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            List<Contacto> contactos = userService.getContactos();
            JSONArray userArray = new JSONArray();
            for (Contacto c : contactos) {
                JSONObject cj = new JSONObject();
                cj.put("nombre", c.getNombre());
                cj.put("email", c.getEmail());
                cj.put("direccion", c.getDireccion());
                cj.put("telefono", c.getTelefono());
                userArray.add(cj);
            }
            output.put("contacto_list", userArray);
        } catch (Exception ex) {
            logger.error("Cannot get Contacto List: ", ex);
            output.put("result", "ERROR");
        } finally {
            out.print(output.toString());
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
