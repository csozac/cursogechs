package cl.gechs.cursojava.service;

/**
 *
 * @author Carlos Soza C <carlos.soza at gnail dot com>
 */
public interface ContactoServiceMXBean {

    public String contactoList();
}
