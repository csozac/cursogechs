package cl.gechs.cursojava.service;

import cl.gechs.cursojava.model.Contacto;
import java.util.List;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos dot com>
 */
@javax.ejb.Local
public interface ContactoServiceLocal {

    public void start();

    public void stop();

    public boolean saveContacto(Contacto user);

    public boolean updateContacto(Contacto user);

    public Contacto getContacto(Long contactoId);

    public List<Contacto> getContactos();
}
