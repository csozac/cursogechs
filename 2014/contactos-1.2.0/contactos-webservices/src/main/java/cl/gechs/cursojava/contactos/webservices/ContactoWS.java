package cl.gechs.cursojava.contactos.webservices;

import cl.gechs.cursojava.contactos.webservices.data.ContactoServiceRequestType;
import cl.gechs.cursojava.contactos.webservices.data.ContactoServiceResponseType;
import cl.gechs.cursojava.contactos.webservices.data.ContactoServiceResultType;
import cl.gechs.cursojava.model.Contacto;
import cl.gechs.cursojava.service.ContactoServiceLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@WebService(
        portName = "ContactoWSPort",
        serviceName = "ContactoWS",
        targetNamespace = "http://www.gechs.cl/webservices/ws-contacto",
        endpointInterface = "cl.gechs.cursojava.contactos.webservices.ContactoWSI")
public class ContactoWS implements ContactoWSI {

    @EJB(mappedName = "java:global/contactos-service/ContactoService!cl.gechs.cursojava.service.ContactoServiceLocal")
    private static ContactoServiceLocal contactoService;

    @Override
    public ContactoServiceResponseType contactoByIdRequest(ContactoServiceRequestType request) {

        Contacto contacto = contactoService.getContacto(Long.parseLong(request.getContactoId()));
        ContactoServiceResponseType response = new ContactoServiceResponseType();
        response.setResult(ContactoServiceResultType.OK);
        response.setDescription(contacto.toString());
        return response;
    }

    @Override
    public ContactoServiceResponseType contactoListRequest() {
        List<Contacto> contactos = contactoService.getContactos();
        String resp = "";
        for (Contacto c : contactos) {
            resp += c + "\n";
        }
        ContactoServiceResponseType response = new ContactoServiceResponseType();
        response.setResult(ContactoServiceResultType.OK);
        response.setDescription(resp);
        return response;
    }
}