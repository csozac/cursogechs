package cl.gechs.cursojava;

import cl.gechs.cursojava.dao.AlternativaDAO;
import cl.gechs.cursojava.dao.EncuestaDAO;
import cl.gechs.cursojava.dao.PreguntaDAO;
import cl.gechs.cursojava.dao.VotoDAO;
import cl.gechs.cursojava.dao.jpa.AlternativaDAOJpa;
import cl.gechs.cursojava.dao.jpa.EncuestaDAOJpa;
import cl.gechs.cursojava.dao.jpa.PreguntaDAOJpa;
import cl.gechs.cursojava.dao.jpa.VotoDAOJpa;
import cl.gechs.cursojava.model.Alternativa;
import cl.gechs.cursojava.model.Encuesta;
import cl.gechs.cursojava.model.Pregunta;
import cl.gechs.cursojava.model.Voto;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail dot>
 */
@RunWith(Arquillian.class)
public class EncuestaDAOTest {

    @EJB
    private EncuestaDAO encuestaDAO;

    @EJB
    private PreguntaDAO preguntaDAO;

    @Deployment(name = "survey-dao", order = 1, testable = true)
    public static WebArchive createDatabaseDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class, "survey-dao.war")
                .addPackage(EncuestaDAO.class.getPackage())
                .addPackage(EncuestaDAOJpa.class.getPackage())
                .addPackage(Encuesta.class.getPackage())
                .addPackage(PreguntaDAO.class.getPackage())
                .addPackage(PreguntaDAOJpa.class.getPackage())
                .addPackage(Pregunta.class.getPackage())
                .addAsResource("persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
        System.out.println(war.toString(true));
        return war;
    }

//    @Test
//    @OperateOnDeployment("survey-dao")
//    @InSequence(1)
//    public void testEncuestaDAOCRUD() {
//        Assert.assertNotNull(this.encuestaDAO);
//
//        Encuesta encuesta = new Encuesta();
//        encuesta.setNombre("Encuesta 01");
//        encuesta.setDescripcion("Encuesta 01 Descripcion");
//
//        //insert
//        Assert.assertTrue(this.encuestaDAO.save(encuesta));
//
//        //update
//        encuesta.setDescripcion("Ecuesta 01 Descripcion Modificada");
//        Assert.assertTrue(this.encuestaDAO.update(encuesta));
//
//        //delete
//        Assert.assertTrue(this.encuestaDAO.delete(encuesta));
//    }
    @Test
    @OperateOnDeployment("survey-dao")
    @InSequence(2)
    public void testEncuestaDAOList() {
        Assert.assertNotNull(this.encuestaDAO);

        Encuesta encuesta = new Encuesta();
        encuesta.setNombre("Encuesta 01");
        encuesta.setDescripcion("Encuesta 01 Descripcion");
        //insert
        Assert.assertTrue(this.encuestaDAO.save(encuesta));

        Set<Pregunta> preguntas = new HashSet<Pregunta>();
        Pregunta p = new Pregunta();
        p.setNombre("PNombre");
        p.setDescripcion("pDecripcion");
        p.setEncuesta(encuesta);
        preguntas.add(p);

        encuesta.setPreguntas(preguntas);

//        //update
        encuesta.setDescripcion("Ecuesta 01 Descripcion Modificada");
        Assert.assertTrue(this.encuestaDAO.update(encuesta));
        
        
//        //delete
//        Assert.assertTrue(this.encuestaDAO.delete(encuesta));
    }
}
