package cl.gechs.cursojava.model;

import java.io.Serializable;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
public enum UserRolEnum implements Serializable {

    ADMINISTRADOR("ADMINISTRADOR", "ADMINISTRADOR", "Administrador"),
    PARTICIPANTE("PARTICIPANTE", "PARTICIPANTE", "Participante");
    public String id;
    public String nemo;
    public String nombre;

    UserRolEnum(String id, String nemo, String nombre) {
        this.id = id;
        this.nemo = nemo;
        this.nombre = nombre;
    }

    public String getId() {
        return this.id;
    }

    public String getNemo() {
        return this.nemo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public static UserRolEnum getById(String idRol) {
        for (UserRolEnum tipo : UserRolEnum.values()) {
            if (tipo.getId().equalsIgnoreCase(idRol)) {
                return tipo;
            }
        }
        return null;
    }

    public static UserRolEnum getByNombre(String nombre) {
        for (UserRolEnum tipo : UserRolEnum.values()) {
            if (tipo.getNombre().equalsIgnoreCase(nombre)) {
                return tipo;
            }
        }
        return null;
    }
}
