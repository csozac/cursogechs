package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.dao.VotacionDAO;
import cl.gechs.cursojava.model.Votacion;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Stateful
@javax.ejb.Local(VotacionDAO.class)
public class VotacionDAOJpa extends BaseDAOJpa<Votacion> implements VotacionDAO {

    private static final Logger logger = Logger.getLogger(VotacionDAOJpa.class);
    @Produces
    @PersistenceContext
    private static EntityManager em;

    @Override
    public Class<Votacion> getObjectClass() {
        return Votacion.class;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Votacion> getVotaciones() {
        Query query = this.getEntityManager().createQuery("FROM Votacion");
        try {
            return (List<Votacion>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public Votacion getVotacion(long idVotacion) {
        try {
            return this.getById(idVotacion);
        } catch (Exception ex) {
            logger.error("Error al recuperar Votacion.", ex);
            return null;
        }
    }
}
