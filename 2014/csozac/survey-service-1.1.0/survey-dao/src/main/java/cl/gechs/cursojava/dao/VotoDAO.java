package cl.gechs.cursojava.dao;

import cl.gechs.cursojava.model.Voto;
import java.util.List;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Local
public interface VotoDAO extends BaseDAO<Voto> {

    public List<Voto> getVotos();

    public Voto getVoto(long idVoto);
}
