package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.dao.EncuestaDAO;
import cl.gechs.cursojava.model.Encuesta;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Stateful
@javax.ejb.Local(EncuestaDAO.class)
public class EncuestaDAOJpa extends BaseDAOJpa<Encuesta> implements EncuestaDAO {

    private static final Logger logger = Logger.getLogger(EncuestaDAOJpa.class);
    @Produces
    @PersistenceContext
    private static EntityManager em;

    @Override
    public Class<Encuesta> getObjectClass() {
        return Encuesta.class;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Encuesta> getEncuestas() {
        Query query = this.getEntityManager().createQuery("FROM Encuesta");
        try {
            return (List<Encuesta>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public Encuesta getEncuesta(long idEncuesta) {
        try {
            return this.getById(idEncuesta);
        } catch (Exception ex) {
            logger.error("Error al recuperar Encuesta.", ex);
            return null;
        }
    }
}
