package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Entity
@Table(name = "user_rol")
public class UserRol implements Serializable {

    private static final long serialVersionUID = 1L;
    private long idRolUsuario;
    private UserRolEnum nombre;
    private long fechaCreacion;

    public UserRol() {
    }

    public UserRol(long idRolUsuario, UserRolEnum nombre, long fechaCreacion) {
        this.idRolUsuario = idRolUsuario;
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id_rol_usuario")
    public long getIdRolUsuario() {
        return idRolUsuario;
    }

    public void setIdRolUsuario(long idRolUsuario) {
        this.idRolUsuario = idRolUsuario;
    }

    @Basic(optional = false)
    @Column(name = "nombre")
    @Enumerated(EnumType.STRING)
    public UserRolEnum getNombre() {
        return nombre;
    }

    public void setNombre(UserRolEnum nombre) {
        this.nombre = nombre;
    }

    @Basic(optional = false)
    @Column(name = "fecha_creacion")
    public long getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(long fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserRol) {
            return ((UserRol) obj).getIdRolUsuario() == this.getIdRolUsuario();
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (this.idRolUsuario ^ (this.idRolUsuario >>> 32));
        return hash;
    }
}
