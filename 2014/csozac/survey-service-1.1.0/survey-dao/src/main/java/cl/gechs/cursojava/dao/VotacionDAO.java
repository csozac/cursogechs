package cl.gechs.cursojava.dao;

import cl.gechs.cursojava.model.Votacion;
import java.util.List;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Local
public interface VotacionDAO extends BaseDAO<Votacion> {

    public List<Votacion> getVotaciones();

    public Votacion getVotacion(long idVotacion);
}
