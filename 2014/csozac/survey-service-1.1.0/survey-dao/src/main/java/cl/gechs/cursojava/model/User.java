package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Entity
@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = "user_id"))
public class User implements Serializable {

    private Long id;

    private String name;

    private String email;

    private String username;

    private String password;

    private boolean enabled;

    private UserRol userRol;

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "user_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic(optional = false)
    @JoinColumn(name = "id_rol_usuario", referencedColumnName = "id_rol_usuario")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public UserRol getUserRol() {
        return userRol;
    }

    public void setUserRol(UserRol userRol) {
        this.userRol = userRol;
    }

    @Basic(optional = false)
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic(optional = false)
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic(optional = false)
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Basic(optional = false)
    @Column(name = "enabled")
    public boolean getEnabled() {
        return enabled;
    }

    @Override
    public String toString() {
        return id + " | " + name + " | " + email + " | " + userRol.getNombre() + " | ";
    }
}
