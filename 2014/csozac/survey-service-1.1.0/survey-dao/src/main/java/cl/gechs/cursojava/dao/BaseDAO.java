package cl.gechs.cursojava.dao;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot dom>
 * @param <T>
 */

public abstract interface BaseDAO<T>
{
  public abstract boolean save(T paramT);

  public abstract boolean update(T paramT);

  public abstract boolean delete(T paramT);

  public abstract boolean delete(Long paramLong);

  public abstract T getById(Long paramLong);
}
