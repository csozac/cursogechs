package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at profondos.com>
 */
@Entity
@Table(name = "pregunta", uniqueConstraints = @UniqueConstraint(columnNames = "pregunta_id"))
public class Pregunta implements Serializable {

    private Long preguntaId;
    private String nombre;
    private String descripcion;
    private Encuesta encuesta;
    //Lista de Preguntas
    private Set<Alternativa> alternativas = new HashSet<Alternativa>(0);

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "pregunta_id")
    public Long getPreguntaId() {
        return preguntaId;
    }

    public void setPreguntaId(Long preguntaId) {
        this.preguntaId = preguntaId;
    }

    @Basic(optional = false)
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic(optional = false)
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "encuesta_id", nullable = false)
    public Encuesta getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(Encuesta encuesta) {
        this.encuesta = encuesta;
    }

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy = "pregunta")
    public Set<Alternativa> getAlternativas() {
        return this.alternativas;
    }

    public void setAlternativas(Set<Alternativa> alternativas) {
        this.alternativas = alternativas;
    }
}
