package  cl.gechs.cursojava.tallermecanico;

/**
 *
 * @author csozac
 */
public class Diagnosticador
{

    /**
     * Método que recibe una referencia a un objeto del tipo Persona y retorna un String con el diagnostico.
     * @param persona
     * @return String con el diagnóstico
     */
    public String diagnosticar(Persona persona)
    {

        Auto auto = persona.getAuto();
        String resp = "";

        //revisar km
        int km = auto.getKilometraje() / 10000;
        int rest = auto.getKilometraje() % 10000;
        if((rest > 9800 || rest < 200))
        {
            if(rest > 9800)
            {
                km++;
            }
            resp += "Su auto tiene " + auto.getKilometraje() + " kms, necesita mantención de los " + km * 10000 + " kms." + "\n";
            return resp;
        }

        //revisar agua
        if(auto.getNivelDeAgua().equalsIgnoreCase("bajo"))
        {
            resp += "\nNivel de agua BAJO, es necesario agregar mas agua al sistema de refrigeración del auto." + "\n";
        } else if(auto.getNivelDeAgua().equalsIgnoreCase("medio"))
        {
            resp += "\nNivel de agua MEDIO, debe estar alerta a los niveles de agua porque el motor se podría calentar." + "\n";
        } else if(auto.getNivelDeAgua().equalsIgnoreCase("alto"))
        {
            resp += "\nNivel de agua ALTO, sistema de refrigeración en buen estado." + "\n";
        } else
        {
            resp += "\nNivel de agua " + auto.getNivelDeAgua() + ", no se puede diagnosticar!." + "\n";
        }
        //revisar aceite
        if(auto.getNivelDeAceite().equalsIgnoreCase("bajo"))
        {
            resp += "Nivel de aceite BAJO, es necesario agregar más aceite al motor." + "\n";
        } else if(auto.getNivelDeAceite().equalsIgnoreCase("medio"))
        {
            resp += "Nivel de aceite MEDIO, debe estar alerta a los niveles de aceite." + "\n";
        } else if(auto.getNivelDeAceite().equalsIgnoreCase("alto"))
        {
            resp += "Nivel de aceite ALTO, aceite en buen estado." + "\n";
        } else
        {
            resp += "Nivel de agua " + auto.getNivelDeAgua() + ", no se puede diagnosticar!." + "\n";
        }

        return resp;

    }
}
