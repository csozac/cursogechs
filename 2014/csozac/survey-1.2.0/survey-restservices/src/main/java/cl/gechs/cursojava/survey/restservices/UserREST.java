package cl.gechs.cursojava.survey.restservices;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.service.UsersServiceLocal;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Path("/user")
public class UserREST {

    private static final Logger logger = Logger.getLogger(UserREST.class);
    private static UsersServiceLocal userService;

    static {
        userService = (UsersServiceLocal) BaseREST.resourceLookup("java:global/survey-service/UsersService!cl.gechs.cursojava.service.UsersServiceLocal");
    }

    @GET
    @Path("/userList")
    @Produces("application/json")
    public Response obtenerUsuarios() {
        JSONObject output = new JSONObject();
        List<User> users = userService.getUsers();
        JSONArray userArray = new JSONArray();
        for (User u : users) {
            JSONObject uj = new JSONObject();
            uj.put("nombre", u.getName());
            uj.put("email", u.getEmail());
            uj.put("rol", u.getUserRol().getNombre().getNombre());
            uj.put("username", u.getUsername());
            userArray.add(uj);
        }
        output.put("user_list", userArray);
        return Response.ok(output.toString()).build();
    }

    @GET
    @Path("/obtener/{userId}")
    @Produces("application/json")
    public Response obtenerUsuario(@PathParam("userId") Long userId) {
        JSONObject output = new JSONObject();
        User u = userService.getUser(userId);
        JSONObject uj = new JSONObject();
        uj.put("nombre", u.getName());
        uj.put("email", u.getEmail());
        uj.put("rol", u.getUserRol().getNombre().getNombre());
        uj.put("username", u.getUsername());
        output.put("user", uj);
        return Response.ok(output.toString()).build();
    }
}
