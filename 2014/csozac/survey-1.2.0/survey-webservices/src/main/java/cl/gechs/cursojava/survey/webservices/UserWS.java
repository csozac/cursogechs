package cl.gechs.cursojava.survey.webservices;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.service.UsersServiceLocal;
import cl.gechs.cursojava.survey.webservices.data.UserServiceRequestType;
import cl.gechs.cursojava.survey.webservices.data.UserServiceResponseType;
import cl.gechs.cursojava.survey.webservices.data.UserServiceResultType;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@WebService(
        portName = "UserWSPort",
        serviceName = "UserWS",
        targetNamespace = "http://www.gechs.cl/webservices/ws-user",
        endpointInterface = "cl.gechs.cursojava.survey.webservices.UserWSI")
public class UserWS implements UserWSI {

    @EJB(mappedName = "java:global/survey-service/UsersService!cl.gechs.cursojava.service.UsersServiceLocal")
    private static UsersServiceLocal userService;

    @Override
    public UserServiceResponseType userByIdRequest(UserServiceRequestType request) {

        User user = userService.getUser(Long.parseLong(request.getUserId()));
        UserServiceResponseType response = new UserServiceResponseType();
        response.setResult(UserServiceResultType.OK);
        response.setDescription(user.toString());
        return response;
    }

    @Override
    public UserServiceResponseType userListRequest() {
        List<User> users = userService.getUsers();
        String resp = "";
        for (User u : users) {
            resp += u + "\n";
        }
        UserServiceResponseType response = new UserServiceResponseType();
        response.setResult(UserServiceResultType.OK);
        response.setDescription(resp);
        return response;
    }
}