package cl.gechs.cursojava.dao;

import java.util.List;
import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Local
public interface UserDAO extends BaseDAO<User> {

    public List<User> getUsers();

    public List<UserRol> getUserRoles();

    public List<User> getUsers(UserRolEnum rol);

    public User getUser(long idUser);

    public User getUserByName(String username);

    public UserRol getUserRolByName(UserRolEnum rol);
}
