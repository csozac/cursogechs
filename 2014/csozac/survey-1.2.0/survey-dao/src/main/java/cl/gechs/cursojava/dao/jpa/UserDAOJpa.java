package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.dao.UserDAO;
import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Stateful
@javax.ejb.Local(UserDAO.class)
public class UserDAOJpa extends BaseDAOJpa<User> implements UserDAO {

    private static final Logger logger = Logger.getLogger(UserDAOJpa.class);
    @Produces
    @PersistenceContext
    private static EntityManager em;

    @Override
    public Class<User> getObjectClass() {
        return User.class;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<UserRol> getUserRoles() {
        Query query = this.getEntityManager().createQuery("FROM UserRol");
        try {
            return (List<UserRol>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public List<User> getUsers() {
        Query query = this.getEntityManager().createQuery("FROM User");
        try {
            return (List<User>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public List<User> getUsers(UserRolEnum rol) {
        Query query = this.getEntityManager().createQuery("FROM User where user.userRol.nombre=:rol");
        query.setParameter("rol", rol);
        try {
            return (List<User>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public User getUser(long idUser) {
        try {
            return this.getById(idUser);
        } catch (Exception ex) {
            logger.error("Error al recuperar Usuarios.", ex);
            return null;
        }
    }

    @Override
    public User getUserByName(String username) {

        Query query = this.getEntityManager().createQuery("FROM User user WHERE user.username=:username");
        query.setParameter("username", username);
        query.setMaxResults(1);
        try {
            return (User) query.getSingleResult();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public UserRol getUserRolByName(UserRolEnum rol) {

        Query query = this.getEntityManager().createQuery("FROM UserRol rol WHERE rol.nombre=:rol");
        query.setParameter("rol", rol);
        query.setMaxResults(1);
        try {
            return (UserRol) query.getSingleResult();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }
}
