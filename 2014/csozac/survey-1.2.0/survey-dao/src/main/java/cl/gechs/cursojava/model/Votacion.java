package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at profondos.com>
 */
@Entity
@Table(name = "votacion", uniqueConstraints = @UniqueConstraint(columnNames = "votacion_id"))
public class Votacion implements Serializable {

    //id
    private Long votacionId;
    //Encuesta
    private Encuesta encuesta;
    //Lista de Votos
    private Set<Voto> votos = new HashSet<Voto>(0);

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "votacion_id")
    public Long getVotacionId() {
        return votacionId;
    }

    public void setVotacionId(Long votacionId) {
        this.votacionId = votacionId;
    }

    @Basic(optional = false)
    @JoinColumn(name = "encuesta_id", referencedColumnName = "encuesta_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    public Encuesta getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(Encuesta encuesta) {
        this.encuesta = encuesta;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "votacion")
    public Set<Voto> getVotos() {
        return this.votos;
    }

    public void setVotos(Set<Voto> votos) {
        this.votos = votos;
    }
}
