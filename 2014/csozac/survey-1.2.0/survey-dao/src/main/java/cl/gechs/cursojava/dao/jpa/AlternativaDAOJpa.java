package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.dao.AlternativaDAO;
import cl.gechs.cursojava.model.Alternativa;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Stateful
@javax.ejb.Local(AlternativaDAO.class)
public class AlternativaDAOJpa extends BaseDAOJpa<Alternativa> implements AlternativaDAO {

    private static final Logger logger = Logger.getLogger(AlternativaDAOJpa.class);
    @Produces
    @PersistenceContext
    private static EntityManager em;

    @Override
    public Class<Alternativa> getObjectClass() {
        return Alternativa.class;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Alternativa> getAlternativas() {
        Query query = this.getEntityManager().createQuery("FROM Alternativa");
        try {
            return (List<Alternativa>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public Alternativa getAlternativa(long idAlternativa) {
        try {
            return this.getById(idAlternativa);
        } catch (Exception ex) {
            logger.error("Error al recuperar Alternativa.", ex);
            return null;
        }
    }
}
