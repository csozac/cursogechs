package cl.gechs.cursojava.dao;

import cl.gechs.cursojava.model.Alternativa;
import java.util.List;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Local
public interface AlternativaDAO extends BaseDAO<Alternativa> {

    public List<Alternativa> getAlternativas();

    public Alternativa getAlternativa(long idAlternativa);
}
