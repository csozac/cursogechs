package cl.gechs.cursojava.dao;

import cl.gechs.cursojava.model.Encuesta;
import java.util.List;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Local
public interface EncuestaDAO extends BaseDAO<Encuesta> {

    public List<Encuesta> getEncuestas();

    public Encuesta getEncuesta(long idEncuesta);
}
