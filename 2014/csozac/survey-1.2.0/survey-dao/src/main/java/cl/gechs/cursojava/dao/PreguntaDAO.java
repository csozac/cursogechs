package cl.gechs.cursojava.dao;

import cl.gechs.cursojava.model.Pregunta;
import java.util.List;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Local
public interface PreguntaDAO extends BaseDAO<Pregunta> {

    public List<Pregunta> getPreguntas();

    public Pregunta getPregunta(long idEncuesta);
}
