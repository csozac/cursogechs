package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.dao.PreguntaDAO;
import cl.gechs.cursojava.model.Pregunta;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Stateful
@javax.ejb.Local(PreguntaDAO.class)
public class PreguntaDAOJpa extends BaseDAOJpa<Pregunta> implements PreguntaDAO {

    private static final Logger logger = Logger.getLogger(PreguntaDAOJpa.class);
    @Produces
    @PersistenceContext
    private static EntityManager em;

    @Override
    public Class<Pregunta> getObjectClass() {
        return Pregunta.class;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Pregunta> getPreguntas() {
        Query query = this.getEntityManager().createQuery("FROM Pregunta");
        try {
            return (List<Pregunta>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public Pregunta getPregunta(long idPregunta) {
        try {
            return this.getById(idPregunta);
        } catch (Exception ex) {
            logger.error("Error al recuperar Pregunta.", ex);
            return null;
        }
    }
}
