package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.dao.VotoDAO;
import cl.gechs.cursojava.model.Voto;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Stateful
@javax.ejb.Local(VotoDAO.class)
public class VotoDAOJpa extends BaseDAOJpa<Voto> implements VotoDAO {

    private static final Logger logger = Logger.getLogger(VotoDAOJpa.class);
    @Produces
    @PersistenceContext
    private static EntityManager em;

    @Override
    public Class<Voto> getObjectClass() {
        return Voto.class;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Voto> getVotos() {
        Query query = this.getEntityManager().createQuery("FROM Voto");
        try {
            return (List<Voto>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public Voto getVoto(long idVoto) {
        try {
            return this.getById(idVoto);
        } catch (Exception ex) {
            logger.error("Error al recuperar Voto.", ex);
            return null;
        }
    }
}
