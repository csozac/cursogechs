package cl.gechs.cursojava.service;

import cl.gechs.cursojava.dao.UserDAO;
import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail dot com>
 */
@Startup
@Singleton
@Local(UsersServiceLocal.class)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class UsersService implements UsersServiceLocal, UsersServiceMXBean {

    private static final Logger logger = Logger.getLogger(UsersService.class);
    private static final SecureRandom random = new SecureRandom();
    //SERVICES
   
    @EJB(mappedName = "java:global/survey-dao/UserDAOJpa!cl.gechs.cursojava.dao.UserDAO")
    private static UserDAO userDAO;
    //MBEAN
    private static MBeanServer platformMBeanServer;
    private static ObjectName objectName = null;

    @Override
    @javax.annotation.PostConstruct
    public void start() {
        logger.info("-------------------------------------------------------------------------");
        logger.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        logger.info("Starting Users Service");

        //register mbean
        try {
            objectName = new ObjectName("survey:services=" + this.getClass().getName());
            platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
            platformMBeanServer.registerMBean(this, objectName);
        } catch (MalformedObjectNameException | InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException e) {
            throw new IllegalStateException("Problem during registration of Monitoring into JMX:" + e);
        }

        logger.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        logger.info("-------------------------------------------------------------------------");
    }

    @Override
    @javax.annotation.PreDestroy
    public void stop() {
        logger.info("-------------------------------------------------------------------------");
        logger.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        logger.info("Ending Users Service");

        //unregister mbean
        try {
            platformMBeanServer.unregisterMBean(objectName);
        } catch (InstanceNotFoundException | MBeanRegistrationException e) {
            throw new IllegalStateException("Problem during unregistration of Monitoring into JMX:" + e);
        }

        logger.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        logger.info("-------------------------------------------------------------------------");
    }

    @Override
    @Lock(LockType.WRITE)
    public boolean saveUser(User user) {
        try {
            //revisar user id
            User euser = userDAO.getUser(user.getId());
            if (euser == null) {
                if (userDAO.save(user)) {
                    return true;
                } else {
                    logger.error("Cannot save User " + user + ". Database error");
                }
            } else {
                logger.error("Cannot save User " + user + ". UserId already registered");
            }
        } catch (Exception ex) {
            logger.error("Cannot save User " + user + ": ", ex);
        }
        return false;
    }

    @Override
    @Lock(LockType.WRITE)
    public boolean updateUser(User user) {
        try {
            //check user id
            User euser = userDAO.getUser(user.getId());
            if (euser != null) {
                if (userDAO.update(user)) {
                    return true;
                } else {
                    logger.error("Cannot update User " + user + ". Database error");
                }
            } else {
                logger.error("Cannot update User " + user + ". UserId not registered");
            }
        } catch (Exception ex) {
            logger.error("Cannot update User " + user + ": ", ex);
        }
        return false;
    }

    @Override
    @Lock(LockType.READ)
    public String generatePassword(Long userId) {
        //get user
        User user = userDAO.getUser(userId);
        if (user != null) {
            //generate password
            String password = this.createPassword();
            //encrypt password and set to the user
            user.setPassword(this.encryptPassword(password));
            if (userDAO.update(user)) {
                return password;
            } else {
                logger.error("Cannot generarte password to User " + userId + ". Database error");
            }
        } else {
            logger.error("Cannot generarte password to User " + userId + ". Entity not found");
        }
        return null;
    }

    @Override
    @Lock(LockType.READ)
    public User getUser(Long userId) {
        return userDAO.getUser(userId);
    }

    @Override
    @Lock(LockType.READ)
    public List<User> getUsers() {
        return userDAO.getUsers();
    }

    @Override
    @Lock(LockType.READ)
    public List<User> getUsers(UserRolEnum role) {
        return userDAO.getUsers(role);
    }

    @Override
    @Lock(LockType.READ)

    public List<UserRol> getUserRoles() {
        return userDAO.getUserRoles();
    }

    @Override
    @Lock(LockType.READ)
    public UserRol getUserRole(UserRolEnum name) {
        return userDAO.getUserRolByName(name);
    }

    @Override
    @Lock(LockType.READ)
    public String encryptUserPassword(String password) {
        return encryptPassword(password);
    }

    private String createPassword() {
        String password = new BigInteger(130, random).toString(32);
        return password.substring(0, 10);
    }

    private String encryptPassword(String password) {
        return new String(Base64.encodeBase64(DigestUtils.md5(password)));
    }

    @Override
    public String usersList() {
        List<User> users = this.getUsers();
        String userList = "";
        for (User u : users) {
            userList += u.toString() + "\n";
        }
        return userList;
    }
}
