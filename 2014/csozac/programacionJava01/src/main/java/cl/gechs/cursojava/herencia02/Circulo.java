package cl.gechs.cursojava.herencia02;

/**
 *
 * @author csozac
 */
public class Circulo extends Figura
{

    public Circulo(String c)
    {
        super(c);
    }

    @Override
    protected void rotar()
    {
        System.out.println("Rotar Circulo de color :" + getColor());
    }

    @Override
    protected void rotar(int grados)
    {
        System.out.println("Rotar " + grados + " grados el Circulo de color :" + getColor());
    }
}
