package cl.gechs.cursojava.fechas;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

/**
 *
 * @author csozac
 */
public class TestAverage
{

    public static void main(String[] args)
    {

        DecimalFormatSymbols symbolsFormat = new DecimalFormatSymbols();
        symbolsFormat.setDecimalSeparator(',');
        DecimalFormat df = new DecimalFormat("#######.##", symbolsFormat);

        ArrayList<Double> datos = new ArrayList<Double>();
        for(int i = 0; i < 10000000; i++)
        {
            double random = Math.random() * 1000;
            datos.add(random);
        }
        AverageResult av = calculateAverage(datos);

        System.out.println("Average: " + df.format(av.getAverage()));
        System.out.println("Time: " + av.getExecutionTime());
    }

    public static AverageResult calculateAverage(ArrayList<Double> datos)
    {
        long executionTime = 0l;
        long initTime = System.currentTimeMillis();
        double average = 0.0;

        double sum = 0.0;
        for(Double d : datos)
        {
            sum += d;
        }
        average = sum / datos.size();
        long finalTime = System.currentTimeMillis();
        executionTime = finalTime - initTime;
        AverageResult av = new AverageResult(average, executionTime);
        return av;
    }
}
