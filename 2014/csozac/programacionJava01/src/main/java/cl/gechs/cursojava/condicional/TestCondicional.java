package cl.gechs.cursojava.condicional;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestCondicional {

    public static void main(String[] args) {
        String status = "";
        int grade = 80;
        //get status of the student
        status = (grade >= 60) ? "Passed" : "Fail";
        //print status
        System.out.println(status);
    }
}
