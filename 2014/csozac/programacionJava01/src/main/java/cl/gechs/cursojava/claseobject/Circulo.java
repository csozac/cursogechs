package cl.gechs.cursojava.claseobject;

/**
 *
 * @author csozac
 */
public class Circulo extends Figura
{

    public Circulo(String color)
    {
        super(color);
    }

    @Override
    public String getColor()
    {
        return this.color;
    }
}
