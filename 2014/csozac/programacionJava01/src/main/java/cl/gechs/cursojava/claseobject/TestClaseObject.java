package cl.gechs.cursojava.claseobject;

/**
 *
 * @author csozac
 */
public class TestClaseObject
{

    public static void main(String[] args)
    {

        Object ca = new Circulo("Amarillo");
        Object tr = new Triangulo("Rojo");

        System.out.println("Clase ca: " + ca.getClass().getName());
        System.out.println("Clase tr: " + tr.getClass().getName());
    }
}