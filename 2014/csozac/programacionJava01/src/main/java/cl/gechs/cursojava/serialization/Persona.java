package cl.gechs.cursojava.serialization;

import java.io.Serializable;

/**
 *
 * @author csozac
 */
public class Persona implements Serializable {

    private String nombre;

    public Persona() {
    }

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
