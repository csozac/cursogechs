package cl.gechs.cursojava.abstractas;

/**
 *
 * @author csozac
 */
public class Circulo extends Figura
{

    public Circulo(String color)
    {
        super(color);
    }

    @Override
    public void rotar()
    {
        //codigo para rotar la figura
    }
}
