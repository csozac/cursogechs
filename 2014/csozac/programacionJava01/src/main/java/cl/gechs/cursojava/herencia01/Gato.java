package cl.gechs.cursojava.herencia01;

/**
 *
 * @author csozac
 */
public class Gato extends Animal
{

    @Override
    public void comer()
    {
        System.out.println("Comer de Gato!");
    }
}
