package cl.gechs.cursojava.interfaces;

/**
 *
 * @author csozac
 */
public class Caballo extends Animal implements MedioDeTransporte, Mascota
{

    private String color;

    public void andar()
    {
        System.out.println("Andar Caballo");
    }

    public void jugar()
    {
        System.out.println("Jugar Caballo");
    }
}
