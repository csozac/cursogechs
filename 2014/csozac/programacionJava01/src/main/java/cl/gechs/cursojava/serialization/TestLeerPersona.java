package cl.gechs.cursojava.serialization;

import java.beans.XMLDecoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author csozac
 */
public class TestLeerPersona {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        try {
            FileInputStream fileInputStream = new FileInputStream(new File("Personas.xml"));
            XMLDecoder os = new XMLDecoder(fileInputStream);
            Persona p1 = (Persona) os.readObject();
            Persona p2 = (Persona) os.readObject();
            os.close();
            System.out.println("P1: " + p1.getNombre());
            System.out.println("P2: " + p2.getNombre());

        } catch (FileNotFoundException fne) {
            System.out.println("Archivo no existe: " + fne.getMessage());

        }
    }
}
