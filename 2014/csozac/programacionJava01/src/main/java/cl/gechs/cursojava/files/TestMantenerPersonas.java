package cl.gechs.cursojava.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author csozac
 */
public class TestMantenerPersonas
{

    private static String fileName = "InventarioPersonas.csv";

    public static void main(String[] args) throws IOException
    {

        leerPersonas();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ingrese nombre:");
        String nombre = bufferedReader.readLine();
        System.out.println("Ingrese rut:");
        String rut = bufferedReader.readLine();

        Persona p = new Persona(nombre, rut);
        guardarPersona(p);

    }

    private static void guardarPersona(Persona p) throws IOException
    {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(fileName), true));
        bufferedWriter.write(p.getNombre() + "," + p.getRut() + "\n");
        bufferedWriter.close();
    }

    private static void leerPersonas() throws FileNotFoundException, IOException
    {
        File ipf = new File(fileName);
        if(ipf.exists())
        {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(ipf));
            String line;
            while((line = bufferedReader.readLine()) != null)
            {
                System.out.println(line);
            }
        }
    }
}
