package cl.gechs.cursojava.alcance;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestAlcance {

    public static void main(String[] args) {
        {
            int i = 10;
            System.out.println("i=" + i);
        }
        {
            int i = 11;
            System.out.println("i=" + i);
        }
        //System.out.println("i=" + i);
    }
}
