package cl.gechs.cursojava.interfaces;

/**
 *
 * @author csozac
 */
public interface MedioDeTransporte {

    public void andar();
}
