package cl.gechs.cursojava.bufferreader;

import java.util.Scanner;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestScanner {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);  //crear un objeto Scanner
        String nombre;
        System.out.print("Introduzca su nombre: ");
        nombre = sc.nextLine();  //leer un String
        System.out.println("Hola " + nombre + "!!!");
    }
}
