package cl.gechs.cursojava.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author csozac
 */
public class TestBufferFileWrite
{

    public static void main(String[] args) throws IOException
    {
        
        FileWriter fileWriter = new FileWriter(new File("personasBR.csv"));
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write("Hola\n");
        bufferedWriter.write("Hola2");
        bufferedWriter.close();
    }
}
