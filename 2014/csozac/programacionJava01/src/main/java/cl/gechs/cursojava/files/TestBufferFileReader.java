package cl.gechs.cursojava.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author csozac
 */
public class TestBufferFileReader
{

    public static void main(String[] args) throws IOException
    {

        FileReader fileReader = new FileReader(new File("personasBR.csv"));
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        System.out.println(bufferedReader.readLine());
        System.out.println(bufferedReader.readLine());
        System.out.println(bufferedReader.readLine());

        bufferedReader.close();
    }
}
