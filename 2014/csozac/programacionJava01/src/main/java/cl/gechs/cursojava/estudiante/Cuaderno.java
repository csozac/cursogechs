package cl.gechs.cursojava.estudiante;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class Cuaderno {

    private String marca;
    private String color;
    private String modelo;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}
