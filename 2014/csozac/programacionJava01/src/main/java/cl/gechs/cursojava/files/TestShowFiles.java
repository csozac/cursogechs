package cl.gechs.cursojava.files;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author csozac
 */
public class TestShowFiles
{

    public static void main(String[] args) throws IOException
    {

        File file = new File("/Volumes/Data/");

        showFiles(file);
    }

    public static void showFiles(File f)
    {
        if(f.isDirectory() && f.listFiles() != null)
        {
            System.out.println("Dir: " + f.getAbsolutePath());
            for(File f1 : f.listFiles())
            {
                showFiles(f1);
            }
        } else
        {
            System.out.println("File: " + f);
        }
    }
}
