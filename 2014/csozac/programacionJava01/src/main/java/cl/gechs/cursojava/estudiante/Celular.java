package cl.gechs.cursojava.estudiante;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class Celular {

    private String marca;
    private String modelo;
    private String color;
    private int saldoEnMinutos;
    private int saldoEnPesos;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSaldoEnMinutos() {
        return saldoEnMinutos;
    }

    public void setSaldoEnMinutos(int saldoEnMinutos) {
        this.saldoEnMinutos = saldoEnMinutos;
    }

    public int getSaldoEnPesos() {
        return saldoEnPesos;
    }

    public void setSaldoEnPesos(int saldoEnPesos) {
        this.saldoEnPesos = saldoEnPesos;
    }
}
