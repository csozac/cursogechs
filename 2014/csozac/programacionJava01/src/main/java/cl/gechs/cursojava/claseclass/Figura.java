package cl.gechs.cursojava.claseclass;

/**
 *
 * @author csozac
 */
public abstract class Figura
{
    protected String color;

    public abstract String getColor();

    public Figura(String color)
    {
        this.color = color;
    }
}
