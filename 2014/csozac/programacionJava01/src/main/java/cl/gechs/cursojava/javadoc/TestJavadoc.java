package cl.gechs.cursojava.javadoc;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestJavadoc {

    private String nombre;
    private String email;

    /**
     * Este m&eacute;todo permite mostrar los elementos de la clase Test.
     * @param rut : Rut del Cliente
     */
    public void mostrarDatos(String rut) {
        System.out.println("Nombre: " + nombre + " Email: " + email + " Rut: " + rut);
    }
}
