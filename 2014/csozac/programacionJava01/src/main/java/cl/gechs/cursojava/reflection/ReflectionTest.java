package cl.gechs.cursojava.reflection;

import java.lang.reflect.*;
import javax.swing.*;

public class ReflectionTest
{

    public static void main(String[] args)
    {
        String name;
        name = JOptionPane.showInputDialog("Ingrese nombre de la clase (e.g. java.util.Date): ");

        try
        {
            // Imprime el nombre de la clase y la superclase (if != Object)
            Class cl = Class.forName(name);
            Class supercl = cl.getSuperclass();
            System.out.print("Clase " + name);
            if(supercl != null && supercl != Object.class)
            {
                System.out.print(" extends " + supercl.getName());
            }

            System.out.print("\n{\n");
            printConstructors(cl);
            System.out.println();
            printMethods(cl);
            System.out.println();
            printFields(cl);
            System.out.println("}");
        } catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        System.exit(0);
    }

    /**
    Imprime todos los constructores de la clase
    @param cl una Clase
     */
    public static void printConstructors(Class cl)
    {
        Constructor[] constructors = cl.getDeclaredConstructors();

        for(int i = 0; i < constructors.length; i++)
        {
            Constructor c = constructors[i];
            String name = c.getName();
            System.out.print("   " + Modifier.toString(c.getModifiers()));
            System.out.print(" " + name + "(");

            // Imprime los tipos de los parametros
            Class[] paramTypes = c.getParameterTypes();
            for(int j = 0; j < paramTypes.length; j++)
            {
                if(j > 0)
                {
                    System.out.print(", ");
                }
                System.out.print(paramTypes[j].getName());
            }
            System.out.println(");");
        }
    }

    /**
    Imprime todos los metodos de la clase
    @param cl una clase
     */
    public static void printMethods(Class cl)
    {
        Method[] methods = cl.getDeclaredMethods();

        for(int i = 0; i < methods.length; i++)
        {
            Method m = methods[i];
            Class retType = m.getReturnType();
            String name = m.getName();

            // imprime los modificadores, tipos de retorno y nombre del metodo
            System.out.print("   " + Modifier.toString(m.getModifiers()));
            System.out.print(" " + retType.getName() + " " + name + "(");

            // Tipos de los parametros
            Class[] paramTypes = m.getParameterTypes();
            for(int j = 0; j < paramTypes.length; j++)
            {
                if(j > 0)
                {
                    System.out.print(", ");
                }
                System.out.print(paramTypes[j].getName());
            }
            System.out.println(");");
        }
    }

    /**
    Imprime todos los campos de la clase
    @param cl una clase
     */
    public static void printFields(Class cl)
    {
        Field[] fields = cl.getDeclaredFields();

        for(int i = 0; i < fields.length; i++)
        {
            Field f = fields[i];
            Class type = f.getType();
            String name = f.getName();
            System.out.print("   " + Modifier.toString(f.getModifiers()));
            System.out.println(" " + type.getName() + " " + name + ";");
        }
    }
}
