package cl.gechs.cursojava.fechas;

/**
 *
 * @author csozac
 */
public class AverageResult
{

    private double average;
    private long executionTime;

    public AverageResult(double average, long executionTime)
    {
        this.average = average;
        this.executionTime = executionTime;
    }

    public double getAverage()
    {
        return average;
    }

    public void setAverage(double average)
    {
        this.average = average;
    }

    public long getExecutionTime()
    {
        return executionTime;
    }

    public void setExecutionTime(long executionTime)
    {
        this.executionTime = executionTime;
    }
}
