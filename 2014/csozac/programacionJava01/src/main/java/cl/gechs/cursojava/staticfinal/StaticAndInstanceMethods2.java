package cl.gechs.cursojava.staticfinal;

/**
 *
 * @author csozac
 */
public class StaticAndInstanceMethods2
{

    public static void main(String[] args)
    {

        String strInstance1 = new String("I am object instance of a String class");

        //char x = String.charAt(2);
        Integer intInstance1 = new Integer(0);
        int x2 = intInstance1.parseInt(new String("35"));
    }
}
