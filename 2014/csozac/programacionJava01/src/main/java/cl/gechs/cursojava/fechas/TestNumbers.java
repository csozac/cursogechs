package cl.gechs.cursojava.fechas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;

/**
 *
 * @author csozac
 */
public class TestNumbers
{

    public static void main(String[] args) throws IOException
    {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String line = br.readLine();
        Double number = Double.parseDouble(line);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(',');
        dfs.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat("'$'#,###.####");//, dfs);
        System.out.println(df.format(number));

        Calendar cal=Calendar.getInstance();
        System.out.println(cal);
        
        Persona p=new Persona();
        System.out.println(p);

    }
}
