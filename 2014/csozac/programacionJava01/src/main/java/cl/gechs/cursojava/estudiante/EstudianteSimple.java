package cl.gechs.cursojava.estudiante;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class EstudianteSimple {

    private String nombre;
    private String rut;
    private int edad;
    //Datos del cuaderno
    private String marcaCuaderno;
    private String colorCuaderno;
    private String modeloCuaderno;
    //Datos del Celular
    private String marcaCelular;
    private String modeloCelular;
    private String colorCelular;
    private int saldoEnMinutos;
    private int saldoEnPesos;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getMarcaCuaderno() {
        return marcaCuaderno;
    }

    public void setMarcaCuaderno(String marcaCuaderno) {
        this.marcaCuaderno = marcaCuaderno;
    }

    public String getColorCuaderno() {
        return colorCuaderno;
    }

    public void setColorCuaderno(String colorCuaderno) {
        this.colorCuaderno = colorCuaderno;
    }

    public String getModeloCuaderno() {
        return modeloCuaderno;
    }

    public void setModeloCuaderno(String modeloCuaderno) {
        this.modeloCuaderno = modeloCuaderno;
    }

    public String getMarcaCelular() {
        return marcaCelular;
    }

    public void setMarcaCelular(String marcaCelular) {
        this.marcaCelular = marcaCelular;
    }

    public String getModeloCelular() {
        return modeloCelular;
    }

    public void setModeloCelular(String modeloCelular) {
        this.modeloCelular = modeloCelular;
    }

    public String getColorCelular() {
        return colorCelular;
    }

    public void setColorCelular(String colorCelular) {
        this.colorCelular = colorCelular;
    }

    public int getSaldoEnMinutos() {
        return saldoEnMinutos;
    }

    public void setSaldoEnMinutos(int saldoEnMinutos) {
        this.saldoEnMinutos = saldoEnMinutos;
    }

    public int getSaldoEnPesos() {
        return saldoEnPesos;
    }

    public void setSaldoEnPesos(int saldoEnPesos) {
        this.saldoEnPesos = saldoEnPesos;
    }
}