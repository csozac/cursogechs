package cl.gechs.cursojava.claseclass;

/**
 *
 * @author csozac
 */
public class TestClaseClass
{

    public static void main(String[] args)
    {

        Figura ca = new Circulo("Amarillo");
        Figura tr = new Triangulo("Rojo");

        System.out.println("Clase ca: " + ca.getClass().getName());
        System.out.println("Clase tr: " + tr.getClass().getName());
    }
}