package cl.gechs.cursojava.staticfinal;

/**
 *
 * @author csozac
 */
public class Calculator
{

    public static int suma(int a, int b)
    {
        return a + b;
    }

    public static int multiplica(int a, int b)
    {
        return a * b;
    }
}
