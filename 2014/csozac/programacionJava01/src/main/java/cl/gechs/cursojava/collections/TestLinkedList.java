package cl.gechs.cursojava.collections;

import java.util.LinkedList;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestLinkedList {

    public static void main(String args[]) {
        // crea un linked list
        LinkedList ll = new LinkedList();
        // agregar elementos a linked list
        ll.add("F");
        ll.add("B");
        ll.add("D");
        ll.add("E");
        ll.add("C");
        ll.addLast("Z");
        ll.addFirst("A");
        ll.add(1, "A2");
        System.out.println("Tamaño original de ll: " + ll);

        // eliminar elmentos
        ll.remove("F");
        ll.remove(2);
        System.out.println("Contenido de  ll despues de eliminar elementos: "
                + ll);

        // remove first and last elements
        ll.removeFirst();
        ll.removeLast();
        System.out.println("ll after deleting first and last: " + ll);

        // obtener y cambiar un valor
        Object val = ll.get(2);
        ll.set(2, (String) val + " Cambiado");
        System.out.println("ll despues del cambio: " + ll);
    }
}
