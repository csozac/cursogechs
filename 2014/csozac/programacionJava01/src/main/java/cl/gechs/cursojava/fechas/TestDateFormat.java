package cl.gechs.cursojava.fechas;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author csozac
 */
public class TestDateFormat
{

    public static void main(String[] args) throws ParseException
    {
        String fecha = "2/7/2011 09:30:01";
        SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yyyy HH:mm:ss");
        System.out.println(dateFormat.parse(fecha));   
    }
}