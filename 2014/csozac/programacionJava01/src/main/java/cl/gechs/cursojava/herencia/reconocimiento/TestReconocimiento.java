package cl.gechs.cursojava.herencia.reconocimiento;

/**
 *
 * @author csozac
 */
public class TestReconocimiento
{

    public static void main(String[] args)
    {



        Persona p1 = null;
        Profesor p2 = new Profesor();
        Alumno p3 = new Alumno();

        if(p1 instanceof Persona)
        {
            System.out.println("P1 es instancia de Persona");
        }
        if(p2 instanceof Profesor)
        {
            System.out.println("P2 es instancia de Profesor");
        }
        if(p3 instanceof Alumno)
        {
            System.out.println("P3 es instancia de Alumno");
        }
        if(p3 instanceof Persona)
        {
            System.out.println("P3 es instancia de Persona");
        }
        if(p2 instanceof Persona)
        {
            System.out.println("P2 es instancia de Persona");
        }
        if(p2 instanceof Object)
        {
            System.out.println("P2 es instancia de Object");
        }
        if(p3 instanceof Object)
        {
            System.out.println("P3 es instancia de Object");
        }
    }
}
