package cl.gechs.cursojava.generics;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestGenerics {
    // Metodo Generic printArray                         

    public static < E> void printArray(E[] inputArray) {
        // Mostrar elementos
        for (E element : inputArray) {
            System.out.printf("%s ", element);
        }
        System.out.println();
    }

    public static void main(String args[]) {
        // Crea arreglo de enteros, double y character
        Integer[] intArray = {1, 2, 3, 4, 5};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4};
        Character[] charArray = {'H', 'E', 'L', 'L', 'O'};

        System.out.println("Arrego de Integer:");
        printArray(intArray); // paso del Arreglo al metodo

        System.out.println("\nArreglo de Doubles:");
        printArray(doubleArray);  // paso del Arreglo al metodo

        System.out.println("\nArreglo de Characters:");
        printArray(charArray);  // paso del Arreglo al metodo
    }
}
