package cl.gechs.cursojava.sobrecarga;

/**
 *
 * @author csozac
 */
public class TestSobrecarga
{

    public static void main(String[] args)
    {
        Calculos calculos = new Calculos();

        System.out.println(calculos.suma(3d, 4d));
        System.out.println(calculos.suma(3, 4));
        System.out.println(calculos.suma(3f, 4f));
    }
}
