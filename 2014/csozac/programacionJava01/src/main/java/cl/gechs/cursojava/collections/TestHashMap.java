package cl.gechs.cursojava.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestHashMap {

    public static void main(String args[]) {

        // Crea un hash map
        HashMap hm = new HashMap();
        // Put  de elementos
        hm.put("Zara", new Double(3434.34));
        hm.put("Mahnaz", new Double(123.22));
        hm.put("Ayan", new Double(1378.00));
        hm.put("Daisy", new Double(99.22));
        hm.put("Qadir", new Double(-19.08));

        // Obtener el conjunto las claves
        Set set = hm.entrySet();
        // Obtener un iterador
        Iterator i = set.iterator();
        // Mostrar elementos
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
        }
        System.out.println();
        // Agregar 1000 a la cuenta de Zara
        double balance = ((Double) hm.get("Zara")).doubleValue();
        hm.put("Zara", new Double(balance + 1000));
        System.out.println("Nueva canitida de Zara: "
                + hm.get("Zara"));
    }
}
