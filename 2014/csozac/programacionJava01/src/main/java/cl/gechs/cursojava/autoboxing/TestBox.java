package cl.gechs.cursojava.autoboxing;

/**
 *
 * @author csozac
 */
public class TestBox {
    Integer i;
    int j;
    public TestBox() {
    }
    public static void main(String[] args) {
        String s=String.format("%1$,9.1f %2$,d", 1000.000,1010010);
	System.out.println(s);

        TestBox t=new TestBox();
        t.go();
    }
    public void go(){
        j=i;
        System.out.println(j);
        System.out.println(i);
    }
}