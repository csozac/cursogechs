package cl.gechs.cursojava.herencia02;

import java.util.ArrayList;

/**
 *
 * @author csozac
 */
public class TestFiguras
{

    public static void main(String[] args)
    {
        ArrayList<Figura> figuras = new ArrayList<Figura>();
        Circulo c = new Circulo("rojo");
        figuras.add(c);
        figuras.add(new Cuadrado("azul"));
        figuras.add(new Triangulo("verde"));
      

        for(Figura f : figuras)
        {
            f.rotar();
            if(f instanceof Triangulo)
            {
                f.rotar(90);
                System.out.println("Clase: " + f.getClass().getSimpleName());
            }
        }
    }
}
