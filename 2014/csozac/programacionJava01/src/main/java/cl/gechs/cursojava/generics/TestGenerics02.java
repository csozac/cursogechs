package cl.gechs.cursojava.generics;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestGenerics02 {

    // Determina el mayor de una lista de objetos comparables
    public static <T extends Comparable<T>> T maximum(T x, T y, T z) {
        T max = x; // asume que x es el mayor
        if (y.compareTo(max) > 0) {
            max = y; // y es el mayor
        }
        if (z.compareTo(max) > 0) {
            max = z; // z es el mayor
        }
        return max; // devuelve el mayor de todos
    }

    public static void main(String args[]) {
        System.out.printf("Max of %d, %d and %d is %d\n\n",
                3, 4, 5, maximum(3, 4, 5));

        System.out.printf("Maxm of %.1f,%.1f and %.1f is %.1f\n\n",
                6.6, 8.8, 7.7, maximum(6.6, 8.8, 7.7));

        System.out.printf("Max of %s, %s and %s is %s\n", "pear",
                "apple", "orange", maximum("pear", "apple", "orange"));
    }
}
