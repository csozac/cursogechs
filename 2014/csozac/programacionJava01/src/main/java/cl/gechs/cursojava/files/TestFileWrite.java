package cl.gechs.cursojava.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author csozac
 */
public class TestFileWrite
{

    public static void main(String[] args) throws IOException
    {
        
        FileWriter fileWriter = new FileWriter(new File("personas.csv"));
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("Hola");
        printWriter.println("Hola2");
        printWriter.close();
    }
}
