package cl.gechs.cursojava.staticfinal;

/**
 *
 * @author csozac
 */
public class TestBloqueStatic
{

    public static void main(String[] args)
    {

        BloquesEstaticos b1 = new BloquesEstaticos();
        BloquesEstaticos b2 = new BloquesEstaticos();

        System.out.println("Hora inicio: " + BloquesEstaticos.horaPrimerObjeto);
    }
}
