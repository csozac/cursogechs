package cl.gechs.cursojava.fechas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author csozac
 */
public class TestFechas
{

    public static void main(String[] agrs) throws IOException
    {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ingrese Fecha [yyyy-MM-dd]: ");
        String fecha = br.readLine();
        try
        {
            Date userDate = dateFormat.parse(fecha);
            Calendar cal = Calendar.getInstance();
            cal.setTime(userDate);
            cal.add(Calendar.DAY_OF_YEAR, -2);

            System.out.println("Usuario ingreso: " + dateFormat.format(userDate));
            System.out.println("Dos días despues: " + dateTimeFormat.format(cal.getTime()));

            System.out.println("Ingrese Fecha [yyyy-MM-dd HH:mm:ss]: ");
            String fechaHora = br.readLine();
            Date userFechaHora = dateTimeFormat.parse(fechaHora);
            Calendar currentTime = Calendar.getInstance();
            long diffUserToCurrentDate = userFechaHora.getTime() - currentTime.getTimeInMillis();
            System.out.println("Diff con el tiempo actual: " + diffUserToCurrentDate + " milliseconds");
        } catch(ParseException pe)
        {
            System.out.println("Formato de fecha invalido!");
        }

    }
}
