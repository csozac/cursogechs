package cl.gechs.cursojava.and;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestAnd {

    public static void main(String[] args) {
        int i = 0;
        int j = 10;
        boolean test = false;
        //demonstrate &&
        test = (i > 10) && (j++ > 9);
        System.out.println(i);
        System.out.println(j);
        System.out.println(test);
        //demonstrate &
        test = (i > 10) & (j++ > 9);
        System.out.println(i);
        System.out.println(j);
        System.out.println(test);
    }
}
