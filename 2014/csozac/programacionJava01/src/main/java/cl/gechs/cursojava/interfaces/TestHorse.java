package cl.gechs.cursojava.interfaces;

/**
 *
 * @author csozac
 */
public class TestHorse
{

    public static void main(String[] args)
    {
        Animal an = new Caballo();
        MedioDeTransporte c = new Caballo();
        c.andar();
        //c.jugar();
        Auto a = new Auto();
        a.andar();

        if(c instanceof Object)
        {
            System.out.println("Es Object");
        }
        if(an instanceof Mascota)
        {
            System.out.println("Es Mascota");
        }
        ((Mascota) c).jugar();
        ((Mascota) an).jugar();
    }
}
