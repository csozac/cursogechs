package cl.gechs.cursojava.arrays;

/**
 *
 * @author csozac
 */
public class Alumno
{

    private String nombre;
    private double nc1;
    private double nc2;

    public Alumno()
    {
    }

    public Alumno(String name, double c1, double c2)
    {
        this.nombre = name;
        this.nc1 = c1;
        this.nc2 = c2;
    }

    public Alumno(String name)
    {
        this.nombre = name;
    }

    public double getNc1()
    {
        return nc1;
    }

    public void setNc1(double nc1)
    {
        this.nc1 = nc1;
    }

    public double getNc2()
    {
        return nc2;
    }

    public void setNc2(double nc2)
    {
        this.nc2 = nc2;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public double getPromedio()
    {
        return (nc1 + nc2) / 2;
    }

    public String getDatosAlumno()
    {
        return nombre + " nc1=" + nc1 + " nc2=" + nc2 + "  promedio= " + getPromedio() + "\n";
    }
}
