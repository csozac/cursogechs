package cl.gechs.cursojava.arrays;

import java.util.ArrayList;

/**
 *
 * @author csozac
 */
public class TestAlumnos {

    public static void main(String[] args) {
        Alumno[] alumnos = new Alumno[3];

        Alumno alumno1 = new Alumno();
        alumno1.setNombre("Marco Aguilar");
        alumno1.setNc1(4.5);
        alumno1.setNc2(6.7);

        Alumno alumno2 = new Alumno();
        alumno2.setNombre("Victor Monsalve");
        alumno2.setNc1(3.3);
        alumno2.setNc2(6.9);

        Alumno alumno3 = new Alumno();
        alumno3.setNombre("Camila Villegas");
        alumno3.setNc1(4.1);
        alumno3.setNc2(5.6);

        alumnos[0] = alumno1;
        alumnos[1] = alumno2;
        alumnos[2] = alumno3;

        for (int i = 0; i < alumnos.length; i++) {
            if (alumnos[i].getNombre().equalsIgnoreCase("camila Villegas")) {
                System.out.println(alumnos[i].getDatosAlumno());
            }
        }

        for(Alumno a : alumnos)
        {
            System.out.println(a.getDatosAlumno());
        }
    }
}
