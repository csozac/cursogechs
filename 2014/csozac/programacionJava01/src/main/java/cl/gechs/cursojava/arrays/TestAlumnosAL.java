package cl.gechs.cursojava.arrays;

import java.util.ArrayList;

/**
 *
 * @author csozac
 */
public class TestAlumnosAL
{

    public static void main(String[] args)
    {
        ArrayList<Alumno> alumnos = new ArrayList<Alumno>();

        Alumno alumno1 = new Alumno();
        alumno1.setNombre("Marco Aguilar");
        alumno1.setNc1(4.5);
        alumno1.setNc2(6.7);

        Alumno alumno2 = new Alumno();
        alumno2.setNombre("Victor Monsalve");
        alumno2.setNc1(3.3);
        alumno2.setNc2(6.9);

        Alumno alumno3 = new Alumno();
        alumno3.setNombre("Camila Villegas");
        alumno3.setNc1(4.1);
        alumno3.setNc2(5.6);

        alumnos.add(alumno1);
        alumnos.add(alumno2);
        alumnos.add(alumno3);

        for(int i = 0; i < alumnos.size(); i++)
        {
            System.out.println(alumnos.get(i).getDatosAlumno());
        }

        for(Alumno a : alumnos)
        {
            System.out.println(a.getDatosAlumno());
        }
    }
}