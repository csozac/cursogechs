package cl.gechs.cursojava.abstractas;

/**
 *
 * @author csozac
 */
public abstract class Figura
{

    protected String color="Rojo";

    public Figura()
    {
    }

    public Figura(String color)
    {
        this.color = color;
    }

    public abstract void rotar();

    public String getColor()
    {
        return this.color;
    }
}
