package cl.gechs.cursojava.misdatos;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestDatos {

    public static void main(String[] args) {
        MisDatos md = new MisDatos();
        md.nombre = "Carlos Soza";
        md.rut = "1249872-4";
        md.mostrarDatos();

        MisDatos md1 = new MisDatos();
        md1.nombre = "Roberto Canales";
        md1.rut = "21371223-8";
        md1.mostrarDatos();
    }
}
