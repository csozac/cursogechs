package cl.gechs.cursojava.claseclass;

/**
 *
 * @author csozac
 */
public class Triangulo extends Figura
{

    public Triangulo(String color)
    {
        super(color);
    }

    @Override
    public String getColor()
    {
        return this.color;
    }
}
