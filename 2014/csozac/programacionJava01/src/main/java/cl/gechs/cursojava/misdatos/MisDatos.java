package cl.gechs.cursojava.misdatos;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class MisDatos {

    public String nombre;
    public String rut;

    public void mostrarDatos() {
        System.out.println("Nombre: " + nombre + " Rut: " + rut);
    }
}
