package cl.gechs.cursojava.pasovalor;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestPasoValor {

    public static void main(String[] agrs) {
        TestPasoValor tpv = new TestPasoValor();
        int n = 23;
        int calculaValor = tpv.calculaValor(n);
        System.out.println("Resultado: " + calculaValor);
        System.out.println("N: " + n);
    }

    public int calculaValor(int n) {
        System.out.println("N: " + n);
        n = 35;
        System.out.println("N: " + n);
        return n *= 2;
    }
}
