package cl.gechs.cursojava.herencia02;

/**
 *
 * @author csozac
 */
public class Triangulo extends Figura
{

    public Triangulo(String c)
    {
        super(c);
    }

    @Override
    protected void rotar()
    {
        System.out.println("Rotar Triangulo de color :" + getColor());
    }

    @Override
    protected void rotar(int grados)
    {
        System.out.println("Rotar " + grados + " grados el Triangulo de color :" + getColor());
    }
}
