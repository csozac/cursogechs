package cl.gechs.cursojava.serialization;

import java.beans.XMLEncoder;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author csozac
 */
public class TestGuadarPersona {

    public static void main(String[] args) throws IOException {

        try {
            FileOutputStream fileOutputStream = new FileOutputStream("Personas.xml", true);
            XMLEncoder os = new XMLEncoder(fileOutputStream);
            Persona p1 = new Persona("Persona1");
            Persona p2 = new Persona("Persona4");
            os.writeObject(p1);
            os.writeObject(p2);
            os.close();

        } catch (FileNotFoundException fne) {
            System.out.println("Archivo no existe: " + fne.getMessage());

        }
    }
}
