package cl.gechs.cursojava.files;

import java.io.File;
import java.util.StringTokenizer;

/**
 *
 * @author csozac
 */
public class TestCreateStructure
{

    public static void main(String[] args)
    {
        createTreeStructure("/hola/hola2/");
    }

    public static void createTreeStructure(String path)
    {
        StringTokenizer token = new StringTokenizer(path, "/");
        String root = "/";
        while(token.hasMoreTokens())
        {
            root += token.nextToken().toString() + "/";
            File file = new File(root);
            if(!file.exists())
            {
                file.mkdir();
                System.out.println(file.getAbsolutePath());
            } else
            {
                System.out.println(file.getAbsolutePath());
            }
        }
    }
}
