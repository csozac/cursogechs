package cl.gechs.cursojava.herencia02;

/**
 *
 * @author csozac
 */
public  abstract class Figura
{

    private String color;

    public Figura()
    {
    }

    public String getColor()
    {
        return color;
    }



    public Figura(String c)
    {
        this.color = c;
    }

    public Figura(int a)
    {
    }

    protected abstract  void rotar();

    protected abstract void rotar(int grados);
}
