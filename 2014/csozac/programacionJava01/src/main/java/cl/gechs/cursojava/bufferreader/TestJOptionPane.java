package cl.gechs.cursojava.bufferreader;

import javax.swing.JOptionPane;


/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestJOptionPane {

    public static void main(String[] args) {
        String nombre;
        nombre = JOptionPane.showInputDialog(null, "Ingrese su Nombre");
        System.out.println("Hola " + nombre + "!!!");
    }
}
