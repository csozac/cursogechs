package cl.gechs.cursojava.herencia01;

/**
 *
 * @author csozac
 */
public class TestHerencia
{

    public static void main(String[] args)
    {
        Animal[] animales = new Animal[2];
        animales[0] = new Perro();
        animales[1] = new Gato();
        for(Animal a : animales)
        {
            a.comer();
            if(a instanceof Animal)
            {
                System.out.println("Si es un Animal");
            }
            if(a instanceof Perro)
            {
                System.out.println("Si es un Perro");
            }

            if(a instanceof Gato)
            {
                System.out.println("Si es un Gato");
            }
            System.out.println(a.getClass().getSimpleName());
        }
    }
}
