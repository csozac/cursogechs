package cl.gechs.cursojava.bufferreader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestBufferReader {

    public static void main(String[] args) throws IOException {
        //BufferedReader para leer de consola
        String nombre;
        BufferedReader leer = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Introduzca su nombre: ");
        nombre = leer.readLine();
        System.out.println("Hola " + nombre + "!!!");

    }
}
