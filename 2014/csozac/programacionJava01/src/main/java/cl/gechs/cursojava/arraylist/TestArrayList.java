package cl.gechs.cursojava.arraylist;

import java.util.ArrayList;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class TestArrayList {

    public static void main(String args[]) {
        ArrayList<String> al = new ArrayList();
        System.out.println("Tamaño Inicial de al: " + al.size());

        // Agregar elementos
        al.add("C");
        al.add("A");
        al.add("E");
        al.add("B");
        al.add("D");
        al.add("F");
        al.add(1, "A2");
        System.out.println("Tamaño de a1 despues de agregar: " + al.size());

        // Motsrar arreglo
        System.out.println("Contenido de al: " + al);
        // Eliminar elementos
        al.remove("F");
        al.remove(2);
        System.out.println("Tamaño de a1 despues de eliminar: " + al.size());
        System.out.println("Contenido de al: " + al);
    }
}
