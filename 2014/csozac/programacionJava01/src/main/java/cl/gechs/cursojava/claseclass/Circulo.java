package cl.gechs.cursojava.claseclass;

/**
 *
 * @author csozac
 */
public class Circulo extends Figura
{

    public Circulo(String color)
    {
        super(color);
    }

    @Override
    public String getColor()
    {
        return this.color;
    }
}
