package cl.gechs.cursojava.staticfinal;

/**
 *
 * @author csozac
 */
public class TestAtStatic
{

    public static void main(String[] args)
    {
        System.out.println(Calculator.suma(3, 4));
        System.out.println(Calculator.multiplica(3, 4));
    }
}
