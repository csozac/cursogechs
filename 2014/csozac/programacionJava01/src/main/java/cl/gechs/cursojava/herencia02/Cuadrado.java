package cl.gechs.cursojava.herencia02;

/**
 *
 * @author csozac
 */
public class Cuadrado extends Figura
{

    public Cuadrado(String c)
    {
        super(c);
    }

    @Override
    protected void rotar()
    {
        System.out.println("Rotar Cuandrado de color :" + getColor());
    }

    @Override
    protected void rotar(int grados)
    {
        System.out.println("Rotar " + grados + " grados el Cuandrado de color :" + getColor());
    }
}
