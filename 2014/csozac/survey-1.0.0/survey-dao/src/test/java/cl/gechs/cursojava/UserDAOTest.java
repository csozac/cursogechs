package cl.gechs.cursojava;

import cl.gechs.cursojava.dao.UserDAO;
import cl.gechs.cursojava.dao.UserRolDAO;
import cl.gechs.cursojava.dao.jpa.UserDAOJpa;
import cl.gechs.cursojava.dao.jpa.UserRolDAOJpa;
import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail dot>
 */
@RunWith(Arquillian.class)
public class UserDAOTest {

    @EJB
    private UserDAO userDAO;
    @EJB
    private UserRolDAO userRolDAO;

    @Deployment(name = "survey-dao", order = 1, testable = true)
    public static WebArchive createDatabaseDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class, "survey-dao.war")
                .addPackage(UserDAO.class.getPackage())
                .addPackage(UserDAOJpa.class.getPackage())
                .addPackage(User.class.getPackage())
                .addPackage(UserRolDAO.class.getPackage())
                .addPackage(UserRolDAOJpa.class.getPackage())
                .addPackage(UserRol.class.getPackage())
                .addAsResource("persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
        System.out.println(war.toString(true));
        return war;
    }

    @Test
    @OperateOnDeployment("survey-dao")
    @InSequence(1)
    public void testUserDAOAddUpdate() {
        Assert.assertNotNull(this.userDAO);

        //Crear Rol
        UserRol ur = new UserRol();
        ur.setFechaCreacion(0l);
        ur.setNombre(UserRolEnum.PARTICIPANTE);
        Assert.assertTrue(this.userRolDAO.save(ur));
        User user = new User();
        //UserRol userRolByName = userDAO.getUserRolByName(UserRolEnum.PARTICIPANTE);
        user.setUserRol(ur);
        user.setName("Carlos Soza");
        user.setPassword("csozac");
        user.setEmail("carlos.soza@gmail.com");
        user.setUsername("csozac");
        user.setEnabled(true);

        //insert
        Assert.assertTrue("Error al guardar el usuario", this.userDAO.save(user));

        //update
        user.setEnabled(Boolean.FALSE);
        Assert.assertTrue(this.userDAO.update(user));
//
//        //delete
        Assert.assertTrue(this.userDAO.delete(user));
        Assert.assertTrue(this.userRolDAO.delete(ur));

    }
}
