package cl.gechs.cursojava.dao.jpa;

import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 * @param <T>
 */
public abstract class BaseDAOJpa<T> {

    private static Logger logger = LoggerFactory.getLogger(BaseDAOJpa.class);

    public abstract Class<T> getObjectClass();

    public abstract EntityManager getEntityManager();

    public boolean save(T entity) {
        try {
            getEntityManager().persist(entity);
            getEntityManager().flush();
            return true;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return false;
    }

    public boolean update(T entity) {
        try {
            getEntityManager().merge(entity);
            getEntityManager().flush();
            return true;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return false;
    }

    public boolean delete(T entity) {
        try {
            Object mentity = getEntityManager().merge(entity);
            getEntityManager().remove(mentity);
            getEntityManager().flush();
            return true;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return false;
    }

    public boolean delete(Long id) {
        try {
            Object obj = getEntityManager().find(getObjectClass(), id);
            getEntityManager().remove(obj);
            getEntityManager().flush();
            return true;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return false;
    }

    public T getById(Long id) {
        try {
            return getEntityManager().find(getObjectClass(), id);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return null;
    }

    public T merge(T entity) {
        try {
            Object mentity = getEntityManager().merge(entity);
            getEntityManager().flush();
            return (T) mentity;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return null;
    }
}
