package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.dao.UserRolDAO;
import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Stateful
@javax.ejb.Local(UserRolDAO.class)
public class UserRolDAOJpa extends BaseDAOJpa<UserRol> implements UserRolDAO {

    private static final Logger logger = Logger.getLogger(UserRolDAOJpa.class);
    @Produces
    @PersistenceContext
    private static EntityManager em;

    @Override
    public Class<UserRol> getObjectClass() {
        return UserRol.class;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<UserRol> getUserRoles() {
        Query query = this.getEntityManager().createQuery("FROM UserRol");
        try {
            return (List<UserRol>) query.getResultList();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }

    @Override
    public UserRol getUserRolByName(UserRolEnum rol) {

        Query query = this.getEntityManager().createQuery("FROM UserRol rol WHERE rol.nombre=:rol");
        query.setParameter("rol", rol);
        query.setMaxResults(1);
        try {
            return (UserRol) query.getSingleResult();
        } catch (javax.persistence.NoResultException ex) {
            return null;
        } catch (Exception ex) {
            logger.error("ERROR DAO: ", ex);
            return null;
        }
    }
}
