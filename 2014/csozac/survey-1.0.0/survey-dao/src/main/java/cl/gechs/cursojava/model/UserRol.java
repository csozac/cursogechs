package cl.gechs.cursojava.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@Entity
@Table(name = "user_rol", uniqueConstraints = @UniqueConstraint(columnNames = "id_rol_usuario"))
public class UserRol implements Serializable {

    private Long idRolUsuario;
    private UserRolEnum nombre;
    private long fechaCreacion;

    public UserRol() {
    }

    public UserRol(UserRolEnum nombre, long fechaCreacion) {
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
    }

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "id_rol_usuario")
    public Long getIdRolUsuario() {
        return idRolUsuario;
    }

    public void setIdRolUsuario(Long idRolUsuario) {
        this.idRolUsuario = idRolUsuario;
    }

    @Basic(optional = false)
    @Column(name = "nombre")
    @Enumerated(EnumType.STRING)
    public UserRolEnum getNombre() {
        return nombre;
    }

    public void setNombre(UserRolEnum nombre) {
        this.nombre = nombre;
    }

    @Basic(optional = false)
    @Column(name = "fecha_creacion")
    public long getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(long fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserRol) {
            return ((UserRol) obj).getIdRolUsuario()== this.getIdRolUsuario();
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (this.idRolUsuario ^ (this.idRolUsuario >>> 32));
        return hash;
    }
}
