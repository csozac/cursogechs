package cl.gechs.cursojava.dao;

import cl.gechs.cursojava.model.UserRol;
import cl.gechs.cursojava.model.UserRolEnum;
import java.util.List;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@javax.ejb.Local
public interface UserRolDAO extends BaseDAO<UserRol> {

    public List<UserRol> getUserRoles();

    public UserRol getUserRolByName(UserRolEnum rol);
}
