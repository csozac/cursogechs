package  cl.gechs.cursojava.tallermecanico;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author csozac
 */
public class TallerMecanico
{

    public static Diagnosticador diagnosticador = new Diagnosticador();
    public static ArrayList<Persona> personas = new ArrayList<Persona>();

    public static void main(String[] args)
    {

        int alternativa;
        Scanner scan = new Scanner(System.in);
        boolean salir = false;
        do
        {
            // Menu principal
            System.out.println("\nIngrese la alternativa:");
            System.out.println("\t1.- Diagnosticar auto");
            System.out.println("\t2.- Mostrar Clientes");
            System.out.println("\t3.- Salir");
            System.out.print("Alternativa: ");
            alternativa = scan.nextInt();
            switch(alternativa)
            {
                case 1:
                    menuDiagnostico();
                    break;
                case 2:
                    mostrarClientes();
                    break;
                case 3:
                    salir = true;
            }
        } while(!salir);
    }

    public static void menuDiagnostico()
    {
        try
        {
            //Declaración e incialización de un Objeto de tipo BufferReader llamado dataIn
            BufferedReader dataIn = new BufferedReader(new InputStreamReader(System.in));

            // DATOS PERSONA
            System.out.println(" Por favor, ingrese datos de la persona:");
            //Ingreso de Nombre
            System.out.print("\nNombre Completo:");
            String name = dataIn.readLine();
            //Ingreso de Rut
            System.out.print("Rut:");
            String rut = dataIn.readLine();
            //Ingreso de Disponible en cuenta corriente
            int disponibleCC = 0;
            boolean disponibleCorrecto = true;
            do
            {
                try
                {
                    System.out.print("Disponible en cuenta corriente:");
                    disponibleCC = Integer.parseInt(dataIn.readLine());
                    if(disponibleCC < 0 || disponibleCC > 100)
                    {
                        System.out.println("Debe ingresar valores entre 0 y 100.");
                        disponibleCorrecto = false;
                    } else
                    {
                        disponibleCorrecto = true;
                    }
                } catch(NumberFormatException ne)
                {
                    System.out.println("Debe ingresar sólo números!.");
                    disponibleCorrecto = false;
                }
            } while(!disponibleCorrecto);

            //Validacion de la cuenta corriente, si es menor a 60 volver al menú principal
            if(disponibleCC < 60)
            {
                System.out.println("Su cuenta corriente tiene " + disponibleCC + " y requiere 60,  no vamos a poder atenderlo. \n");
                return;
            }
            //Ingreso de Disponible en cuenta corriente
            System.out.println("");

            //DATOS DEL AUTO
            System.out.print(" \nPor favor, ingrese datos del auto:");
            //Ingreso de Marca
            System.out.print("\nMarca:");
            String marca = dataIn.readLine();
            //Ingreso de Modelo
            System.out.print("Modelo:");
            String modelo = dataIn.readLine();
            //Ingreso de Color
            System.out.print("Color:");
            String color = dataIn.readLine();
            //Ingreso de Número de Puertas
            boolean puertasCorrecto = true;
            int numeroDePuertas = 0;
            do
            {
                try
                {
                    System.out.print("Puertas[3 o 5]:");
                    numeroDePuertas = Integer.parseInt(dataIn.readLine());
                    if(numeroDePuertas != 3 && numeroDePuertas != 5)
                    {
                        System.out.println("Debe ingresar la opción 3 o 5.");
                        puertasCorrecto = false;
                    } else
                    {
                        puertasCorrecto = true;
                    }
                } catch(NumberFormatException ne)
                {
                    System.out.println("Debe ingresar sólo números!.");
                    puertasCorrecto = false;
                }
            } while(!puertasCorrecto);

            //Ingreso de Número de Kilometraje
            boolean kmCorrecto = true;
            int kilometraje = 0;
            do
            {
                try
                {
                    System.out.print("Kilometraje[0 a 100000]:");
                    kilometraje = Integer.parseInt(dataIn.readLine());
                    if(kilometraje < 0 || kilometraje > 100000)
                    {
                        System.out.println("Debe ingresar valores entre 0 y 100000.");
                        kmCorrecto = false;
                    } else
                    {
                        kmCorrecto = true;
                    }
                } catch(NumberFormatException ne)
                {
                    System.out.println("Debe ingresar sólo números!.");
                    kmCorrecto = false;
                }
            } while(!kmCorrecto);

            //Ingreso de Número de Nivel de Agua
            String nivelAgua;
            do
            {
                System.out.print("Nivel de agua[bajo,medio,alto]:");
                nivelAgua = dataIn.readLine();
                if(!nivelAgua.equalsIgnoreCase("bajo") && !nivelAgua.equalsIgnoreCase("medio") && !nivelAgua.equalsIgnoreCase("alto"))
                {
                    System.out.println("Debe ingresar bajo, medio o alto");
                }
            } while(!nivelAgua.equalsIgnoreCase("bajo") && !nivelAgua.equalsIgnoreCase("medio") && !nivelAgua.equalsIgnoreCase("alto"));

            //Ingreso de Número de Nivel de Aceite
            String nivelAceite;
            do
            {
                System.out.print("Nivel de aceite[bajo,medio, alto]:");
                nivelAceite = dataIn.readLine();
                if(!nivelAceite.equalsIgnoreCase("bajo") && !nivelAceite.equalsIgnoreCase("medio") && !nivelAceite.equalsIgnoreCase("alto"))
                {
                    System.out.println("Debe ingresar bajo, medio o alto");
                }
            } while(!nivelAceite.equalsIgnoreCase("bajo") && !nivelAceite.equalsIgnoreCase("medio") && !nivelAceite.equalsIgnoreCase("alto"));

            // Creación del objeto auto, usando el constructor
            Auto auto = new Auto(marca, modelo, color, numeroDePuertas, nivelAceite, nivelAgua, kilometraje);
            // Lo mismo usando setters
            //Auto auto=new Auto();
            //auto.setMarca(marca);
            //auto.setModelo(modelo);
            //auto.setColor(color);
            //auto.setNumeroDePuertas(numeroDePuertas);
            //auto.setNivelDeAgua(nivelAgua);
            //auto.setNivelDeAceite(nivelAceite);
            //auto.setKilometraje(kilometraje);

            //Creación del objeto persona usando el constructor
            Persona persona = new Persona(name, rut, disponibleCC, auto);
            //Lo mismo pero con setters
            //Persona persona = new Persona();
            //persona.setNombre(name);
            //persona.setRut(rut);
            //persona.setDisponibleCC(disponibleCC);
            //persona.setAuto(auto);

            // LLamada al metodo diagnosticar() de la clase Diagnosticador y asignarlo a un String llamado diagnostico.
            String diagnostico = diagnosticador.diagnosticar(persona);
            //Seteo de el diagnostico a la persona.
            persona.setDiagnostico(diagnostico);
            //Agregar a la persona a la lista de personas atendidas.
            personas.add(persona);
            // Imprimir el diagnóstico.
            System.out.println("Diagnostico: " + persona.getDiagnostico());

        } catch(Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void mostrarClientes()
    {
        if(personas.size() > 0)
        {
            System.out.println("\nExisten " + personas.size() + " personas registradas:");
            for(Persona persona : personas)
            {
                System.out.println(persona.mostrarDatos());
            }
        } else
        {
            System.out.println("NO hay personas registradas!");
        }
    }
}
