<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@page import="com.holdingsmonitor.webapp.servlets.BaseServlet" %>
<%
    String ambiente = BaseServlet.getAppEnviroment();
    String fileType = ambiente.equalsIgnoreCase("DEV") ? "" : ".min";
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico" />
        <!-- css externos -->
        <link type="text/css" href="<%=request.getContextPath()%>/css/custom-theme/jquery-ui-1.9.1.custom.min.css" rel="stylesheet" />
        <link type="text/css" href="<%=request.getContextPath()%>/css/datatable/profondos.datatable<%=fileType%>.css" rel="stylesheet" />
        <link type="text/css" href="<%=request.getContextPath()%>/css/datatable/demos<%=fileType%>.css" rel="stylesheet" />
        <link type="text/css" href="<%=request.getContextPath()%>/css/profondos<%=fileType%>.css" rel="stylesheet" />
        <link type="text/css" href="<%=request.getContextPath()%>/css/prod<%=fileType%>.css" rel="stylesheet" />

        <!-- js externo -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/jquery.blockUI<%=fileType%>.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui/jquery-ui-1.9.0.custom.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/globalize<%=fileType%>.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/globalize.culture.de-DE<%=fileType%>.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/jquery.time.spinner<%=fileType%>.js"></script>


        <!-- test librerias implementar patron MVC backbone -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/backbone/underscore<%=fileType%>.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/backbone/backbone<%=fileType%>.js"></script>


        <!-- test plugin load file -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/ajaxfileupload<%=fileType%>.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/jquery.form<%=fileType%>.js"></script>

        <script type="text/javascript" src="<%=request.getContextPath()%>/js/datatable/jquery.dataTables<%=fileType%>.js"></script>    
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/datatable/jquery.datatable.sort<%=fileType%>.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/jquery.price_format.1.6.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/jquery.a-tools-1.5.2.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/jquery.maskedinput.min.js"></script>

        <!-- idioma calendar -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/i18n/jquery.ui.datepicker-es<%=fileType%>.js" ></script>

        <!-- Holding Monitor JS-->

        <script type="text/javascript" src="<%=request.getContextPath()%>/js/holdingsmonitor/home/hm.init<%=fileType%>.js"></script>
        
        <!-- servlet -->
        <script type="text/javascript">
            var servletLogout = "<%=request.getContextPath()%>/servlets/LogoutUserServlet";
        </script>

        <title>Holding Monitor v1.0.0</title>      
    </head>

    <body style="">
        <div id="contenedor">

            <!-- HEADER -->
            <div id="header" style="border-bottom: medium none; background: none repeat scroll 0% 0% #43474A;">
                <div id="feedback-set-idioma"></div>
                <table border="0" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td style="min-width:500px; width: 40%;">
                                <div class="iconMano" id="logo-profondo"></div>
                            </td>
                            <td>
                                <span class="profile prof-t">USUARIO:</span>
                                <span class="profile_name prof">
                                    &nbsp;
                                    <b id="header-username">PROFondos Admin</b>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    |
                                </span>
                            </td>
                            <td>
                                <span class="profile_name prof">
                                    &nbsp;PROFondos<b id="header-company"></b>
                                    &nbsp;&nbsp;
                                    |
                                </span>
                            </td>
                            <td>
                                <span class="profile prof-t">PERFIL:</span>
                                <span class="profile_name prof">
                                    &nbsp;
                                    <b id="header-perfil">Admin</b>
                                    &nbsp;&nbsp;
                                    |
                                </span>
                            </td>

                            <!--  no visible para version 3.5 -->
                            <td class="profile" style="text-align: center">
                                <img src="<%=request.getContextPath()%>/images/idioma_spn.png" width="16" height="11" alt="idioma_spn" style="cursor: pointer;"/>&nbsp;ESP&nbsp;&nbsp;
                                <img src="<%=request.getContextPath()%>/images/idioma_eng.png" width="16" height="11" alt="idioma_eng" style="cursor: pointer;"/>&nbsp;ENG
                                <img src="<%=request.getContextPath()%>/images/idioma_fr.png" width="16" height="11" alt="idioma_fr" style="cursor: pointer;" />&nbsp;FRA
                                &nbsp;&nbsp;<span class="profile_name">|</span>
                            </td><!-- -->

                            <!-- no visible para version 3.5 -->
                            <td style="text-align: center">
                                <img src="<%=request.getContextPath()%>/images/contact.png" width="17" height="13" alt="contacto" style="cursor:pointer;"/>
                                &nbsp;&nbsp;<span class="profile_name">|</span>
                            </td>
                            <!-- -->
                            <td style="text-align: center">
                                <a href="#" onClick="logout();" class="btn">Cerrar&nbsp;Sesi�n</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END HEADER -->

            <!-- MENU -->
            <div id="menu">
                <div id="content-menu">
                    <div id="menu-buscar"><br>
                        <table border="0" cellspacing="5" cellpadding="2" class="tabla-menu">
                            <tbody>
                                <tr>
                                    <td class="itemprincipal">
                                        <span>BUSCAR</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="itemsecundario">
                                        <img width="5" height="10" alt="NewtriangleRight" src="<%=request.getContextPath()%>/images/bullet-menu.png" class="img-menu" style="display: none;">
                                        <a class="link-menu" id="menu-btn-blotter-ordenes" href="#" style="color: rgb(255, 255, 255);">REPORTES</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="itemsecundario">
                                        <img width="5" height="10" alt="NewtriangleRight" src="<%=request.getContextPath()%>/images/bullet-menu.png" class="img-menu" style="display: none;">
                                        <a class="link-menu" id="menu-btn-buscar-fondo" href="#" style="color: rgb(255, 255, 255);">JOBS</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="itemsecundario">
                                        <img width="5" height="10" alt="NewtriangleRight" src="<%=request.getContextPath()%>/images/bullet-menu.png" class="img-menu" style="display: none;">
                                        <a class="link-menu" id="buscar-eventos" href="#" style="color: rgb(255, 255, 255);">INSTANCIAS</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="itemsecundario"><a class="link-menu" id="buscar-eventos2" href="#" style="color: rgb(255, 255, 255);">USUARIOS</a></td>
                                </tr>  
                            </tbody>
                        </table>
                    </div>
                    <div id="menu-ingresar">
                        <table border="0" cellspacing="5" cellpadding="2" class="tabla-menu">
                            <tbody>
                                <tr>
                                    <td class="itemprincipal">
                                        <span>INGRESAR</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="itemsecundario">
                                        <img width="5" height="10" alt="NewtriangleRight" src="<%=request.getContextPath()%>/images/bullet-menu.png" class="img-menu" style="display: none;">
                                        <a id="menu-btn-ingresar-orden" href="#" class="link-menu" style="color: rgb(255, 255, 255);">REPORTES</a></td>
                                </tr>
                                <tr>
                                    <td class="itemsecundario">
                                        <img width="5" height="10" alt="NewtriangleRight" src="<%=request.getContextPath()%>/images/bullet-menu.png" class="img-menu" style="display: none;">
                                        <a id="menu-btn-load-ord-masiva" href="#" class="link-menu" style="color: rgb(255, 255, 255);">USUARIOS</a> <!-- class="link-menu" -->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END MENU -->

            <!-- CONTENIDO -->
            <div id="contenido">
                <div style="padding: 20px 30px; display: none;" class="contenedores">

                </div>
            </div>
            <!-- END CONTENIDO -->

            <!-- FOOTER -->
            <div id="footer">
                <span>
                    CR�DITOS&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;POL�TICAS&nbsp;DE PRIVACIDAD&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;T�RMINOS Y CONDICIONES
                </span>
            </div>
            <!-- END FOOTER -->
        </div>
    </body>
</html>
