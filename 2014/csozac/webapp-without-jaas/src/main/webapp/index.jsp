<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@page import="com.holdingsmonitor.webapp.servlets.BaseServlet"  %>
<%
    String fileType = BaseServlet.getAppEnviroment().equalsIgnoreCase("DEV") ? "" : ".min";
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico" />
        <link type="text/css" href="<%=request.getContextPath()%>/css/profondos.login<%=fileType%>.css" rel="stylesheet" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery/jquery-1.8.2.min.js"></script>
        <link type="text/css" href="<%=request.getContextPath()%>/css/profondos<%=fileType%>.css" rel="stylesheet" />
        <link type="text/css" href="<%=request.getContextPath()%>/css/prod<%=fileType%>.css" rel="stylesheet" />
        <link type="text/css" href="<%=request.getContextPath()%>/css/prod_login<%=fileType%>.css" rel="stylesheet" />

        <script type="text/javascript">
            function enviar() {
                $('#j_username').val(escape($('#username').val()));
                $("#login_form").submit();
            }
            function checkEnter(e) {
                var characterCode = null;
                if (e && e.which) {
                    e = e;
                    characterCode = e.which;
                } else {
                    if (navigator.appName == 'Microsoft Internet Explorer') {
                        e = event;
                        characterCode = e.keyCode;
                    }
                }

                if (characterCode == 13) {
                    enviar();
                    return false;
                } else {
                    return true;
                }
            }

        </script>
        <title>Holding Monitor v1.0.0</title>
    </head>

    <body>
        <div style="height:735px; background:none repeat scroll 0 0 #627A7D;" id="contenedor">
            <div id="header"><!-- header -->
                <div class="iconMano" id="logo-profondo"></div>
                <div class="head-tool-btn"></div> <!-- mas a la izq-->

                <div class="head-tool">
                    <span class="profile_name prof">
                        &nbsp;<b id="header-perfil"></b>&nbsp;
                    </span>
                </div>
            </div><!-- end header -->
            <div id="contenido-login">               
                <form id="login_form" name="login_form" action="<%=request.getContextPath()%>/servlets/LoginUserServlet" method="post">
                    <table width="239" border="0" cellspacing="0" cellpadding="0" style="font:13px Arial; background:#5A7073;">
                        <tbody>
                            <tr>
                                <td width="83" align="center" style="border:none; color:#fff; font-size:22px;font-weight:bold;font-style:normal;text-decoration:none;color:#FFFFFF;">
                                    Ingreso
                                </td>
                            </tr>
                            <tr>
                                <td height="30" align="center" style="border:none;" colspan="2" id="msgError">&nbsp;</td>
                            </tr>
                            <tr valign="bottom">
                                <td width="167" align="center" style="border:none; ">                                    
                                    <input type="text" onKeyPress="checkEnter(event);" length="17" style="width:130px;" onFocus="if (this.value == 'Usuario')
                    this.value = ''" value="Usuario" name="username" id="username">
                                    <input type="hidden" id="j_username" name="j_username" value="Usuario">
                                </td>
                            </tr>
                            <tr valign="bottom">

                                <td width="167" align="center" style="border:none; ">
                                    <input type="password" onKeyPress="checkEnter(event);" length="17" style="width:130px;" onFocus="if (this.value == 'Contraseña')
                    this.value = ''" value="Contraseña" name="j_password" id="j_password">
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2" style="border: none; text-align: center;">
                                    <span style="color: #fff;">
                                        <%
                                            if (request.getAttribute("errorCode") != null) {

                                                String msg = (String) request.getAttribute("errorCode");
                                                String mensaje = "";
                                                String check = "";
                                                if (msg.equals("ERROR_PASSWORD_NOT_VALID")) {
                                                    mensaje = "El Password ingresado no es v&aacute;lido.";
                                                } else if (msg.equals("ERROR_NOT_ACTIVE_USER")) {
                                                    mensaje = "No se puede iniciar sesi&oacute;n. El usuario ha sido deshabilitado.";
                                                } else if (msg.equals("ERROR_USER_NOT_REGISTERED")) {
                                                    mensaje = "No se puede iniciar sesi&oacute;n. El usuario ingresado no existe.";
                                                } else if (msg.equals("ERROR_MISSING_DATA")) {
                                                    mensaje = "No se puede iniciar sesi&oacute;n. Debe ingresar todos los datos.";
                                                } else if (msg.equals("ERROR_ALREADY_OPENED")) {
                                                    check = "<input type='checkbox' name='force_login' value='true' /><label style='margin-left:-23px;'><strong>Forzar Ingreso.</strong></label><br/>No se puede iniciar sesi&oacute;n. Existe una sesi&oacute;n activa con las mismas credenciales.";
                                                }
                                                out.println(check);
                                                out.println(mensaje);
                                            }
                                        %>
                                    </span>
                                </td>
                            </tr>
                            <tr align="right">
                                <td height="44" align="center" colspan="2" style="border:none; padding: 1px;">
                                    <a class="btn" onClick="enviar();" href="#">Ingresar</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div style="width: 240px; margin: auto; ">                    
                <div style="text-align:center; color: #425457; font-size:11px; margin-top: 30px;">
                    Si olvido su clave llámenos al <font class="fono">+56 2 2616 5613</font><!-- (562) 3398401 -->
                    <br> o escríbanos a <a href="mailto:soporte@profondos.com">soporte</a>.
                    <br>
                    <br>
                </div>
            </div>
        </div>

        <div style="display: none;" title="Información" class="contenido-modal" id="dialog-confirm-pro-modal">
            <p id="mensaje-dialogo-confirmar" class="contenido-modal"></p>
        </div>
    </body>
</html>
