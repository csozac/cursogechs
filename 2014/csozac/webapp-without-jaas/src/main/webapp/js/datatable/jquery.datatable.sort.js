jQuery.fn.dataTableExt.oSort['natural-asc']  = function(a,b) {
    return naturalSort(a,b);
};

jQuery.fn.dataTableExt.oSort['natural-desc'] = function(a,b) {
    return naturalSort(a,b) * -1;
};

/*
 * Natural Sort algorithm for Javascript
 *  Version 0.3
 * Author: Jim Palmer (based on chunking idea from Dave Koelle)
 *  optimizations and safari fix by Mike Grier (mgrier.com)
 * Released under MIT license.
 */
function naturalSort(a, b){
	// setup temp-scope variables for comparison evauluation
	var re = /(-?[0-9\.]+)/g,
		x = a.toString().toLowerCase() || '',
		y = b.toString().toLowerCase() || '',
		nC = String.fromCharCode(0),
		xN = x.replace( re, nC + '$1' + nC ).split(nC),
		yN = y.replace( re, nC + '$1' + nC ).split(nC),
		xD = (new Date(x)).getTime(),
		yD = xD ? (new Date(y)).getTime() : null;
	// natural sorting of dates
	if ( yD )
		if ( xD < yD ) return -1;
		else if ( xD > yD )	return 1;
	// natural sorting through split numeric strings and default strings
	for( var cLoc = 0, numS = Math.max(xN.length, yN.length); cLoc < numS; cLoc++ ) {
		oFxNcL = parseFloat(xN[cLoc]) || xN[cLoc];
		oFyNcL = parseFloat(yN[cLoc]) || yN[cLoc];
		if (oFxNcL < oFyNcL) return -1;
		else if (oFxNcL > oFyNcL) return 1;
	}
	return 0;
}

jQuery.fn.dataTableExt.oSort['formatted-num-asc'] = function(x,y){
    if(x=="-"){
        x = "0";
    }
    if(y=="-"){
        y = "0";
    }
    x = x.replace( /<.*?>/g, "" );
    x = x.replace(/[^\d\-\,\/]/g,'');
    x = x.replace(/,/gi, ".");
    y = y.replace( /<.*?>/g, "" );
    y = y.replace(/[^\d\-\,\/]/g,'');
    y = y.replace(/,/gi, ".");
    if(x.indexOf('/')>=0)x = eval(x);
    if(y.indexOf('/')>=0)y = eval(y);
    return x/1 - y/1;
}
jQuery.fn.dataTableExt.oSort['formatted-num-desc'] = function(x,y){
    if(x=="-"){
        x = "0";
    }
    if(y=="-"){
        y = "0";
    }
    x = x.replace( /<.*?>/g, "" );
    x = x.replace(/[^\d\-\,\/]/g,'');
    x = x.replace(/,/gi, ".");
    y = y.replace( /<.*?>/g, "" );
    y = y.replace(/[^\d\-\,\/]/g,'');
    y = y.replace(/,/gi, ".");
    if(x.indexOf('/')>=0)x = eval(x);
    if(y.indexOf('/')>=0)y = eval(y);
    return y/1 - x/1;
}

jQuery.fn.dataTableExt.oSort['datetime-asc']  = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    var ukDatea = a.split('/');
    var ukDateb = b.split('/');

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['datetime-desc'] = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    var ukDatea = a.split('/');
    var ukDateb = b.split('/');

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
};

jQuery.fn.dataTableExt.oSort['datetimeGuion-asc']  = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    a = a.replace( new RegExp('&nbsp;', 'g'), "" );
    b = b.replace( /<.*?>/g, "" );
    b = b.replace( new RegExp('&nbsp;', 'g'), "" );
    var ukDatea = a.split('-');
    var ukDateb = b.split('-');

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['datetimeGuion-desc'] = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    a = a.replace( new RegExp('&nbsp;', 'g'), "" );
    b = b.replace( /<.*?>/g, "" );
    b = b.replace( new RegExp('&nbsp;', 'g'), "" );
    var ukDatea = a.split('-');
    var ukDateb = b.split('-');

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
};

jQuery.fn.dataTableExt.oSort['fecha-asc'] = function(a, b) {
    a = a.replace(/<.*?>/g, "");
    b = b.replace(/<.*?>/g, "");
    var ukDatea = a.split('-');
    var ukDateb = b.split('-');
    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;
    if (_.isNaN(y)) {
        y = (x * 100);
    }
    if (_.isNaN(x)) {
        x = (y * 100);
    }
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
};

jQuery.fn.dataTableExt.oSort['fecha-desc'] = function(a, b) {
    a = a.replace(/<.*?>/g, "");
    b = b.replace(/<.*?>/g, "");
    var ukDatea = a.split('-');
    var ukDateb = b.split('-');
    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;
    if (_.isNaN(y)) {
        y = (x / 100);
    }
    if (_.isNaN(x)) {
        x = (y / 100);
    }
    return  ((x < y) ? 1 : ((x > y) ? -1 : 0));
};

/*
jQuery.fn.dataTableExt.oSort['fecha-asc']  = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    var ukDatea = a.split('-');
    var ukDateb = b.split('-');

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['fecha-desc'] = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    var ukDatea = a.split('-');
    var ukDateb = b.split('-');

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
};
*/

jQuery.fn.dataTableExt.oSort['fechaHoraCL-asc']  = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    var ukDatea = a.split('-');
    var ukDateb = b.split('-');
    
    if(ukDatea[2]!=null){
        ukDatea[2] = ukDatea[2].replace(/ /g,"")
        ukDatea[2] = ukDatea[2].replace(/:/g,"")
    }

    if(ukDateb[2]!=null){
        ukDateb[2] = ukDateb[2].replace(/ /g,"")
        ukDateb[2] = ukDateb[2].replace(/:/g,"")
    }

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['fechaHoraCL-desc'] = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    var ukDatea = a.split('-');
    var ukDateb = b.split('-');
    
    if(ukDatea[2]!=null){
        ukDatea[2] = ukDatea[2].replace(/ /g,"")
        ukDatea[2] = ukDatea[2].replace(/:/g,"")
    }

    if(ukDateb[2]!=null){
        ukDateb[2] = ukDateb[2].replace(/ /g,"")
        ukDateb[2] = ukDateb[2].replace(/:/g,"")
    }

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
};

jQuery.fn.dataTableExt.oSort['htmlNatural-asc']  = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    return naturalSort(a,b);
};

jQuery.fn.dataTableExt.oSort['htmlNatural-desc'] = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    return naturalSort(a,b) * -1;
};

jQuery.fn.dataTableExt.oSort['htmldatetime-asc']  = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    var ukDatea = a.split('/');
    var ukDateb = b.split('/');

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['htmldatetime-desc'] = function(a,b) {
    a = a.replace( /<.*?>/g, "" );
    b = b.replace( /<.*?>/g, "" );
    var ukDatea = a.split('/');
    var ukDateb = b.split('/');

    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

    return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
};


//-----------------------------------------------------------------------------------------------
// Prueba ordenamiento  formato fecha dd/MM/yyyy  mas hora hh:mm:ss
//-----------------------------------------------------------------------------------------------

function trimX(str) {
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}
 
jQuery.fn.dataTableExt.oSort['date-euro-asc'] = function(a, b) {
    if (trimX(a) != '') {
        var frDatea = trimX(a).split(' ');
        var frTimea = frDatea[1].split(':');
        var frDatea2 = frDatea[0].split('/');
        var x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
    } else {
        var x = 10000000000000; // = l'an 1000 ...
    }
 
    if (trimX(b) != '') {
        var frDateb = trimX(b).split(' ');
        var frTimeb = frDateb[1].split(':');
        frDateb = frDateb[0].split('/');
        var y = (frDateb[2] + frDateb[1] + frDateb[0] + frTimeb[0] + frTimeb[1] + frTimeb[2]) * 1;                     
    } else {
        var y = 10000000000000;                    
    }
    var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
    return z;
};
 
jQuery.fn.dataTableExt.oSort['date-euro-desc'] = function(a, b) {
    if (trimX(a) != '') {
        var frDatea = trimX(a).split(' ');
        var frTimea = frDatea[1].split(':');
        var frDatea2 = frDatea[0].split('/');
        var x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;                      
    } else {
        var x = 10000000000000;                    
    }
 
    if (trimX(b) != '') {
        var frDateb = trimX(b).split(' ');
        var frTimeb = frDateb[1].split(':');
        frDateb = frDateb[0].split('/');
        var y = (frDateb[2] + frDateb[1] + frDateb[0] + frTimeb[0] + frTimeb[1] + frTimeb[2]) * 1;                     
    } else {
        var y = 10000000000000;                    
    }                  
    var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));                  
    return z;
};

//-----------------------------------------------------------------------------------------------

jQuery.fn.dataTableExt.oSort['date-euro2-asc'] = function(a, b) {
    if (trimX(a) != '') {
        var frDatea = trimX(a).split(' ');
        var frTimea = frDatea[1].split(':');
        var frDatea2 = frDatea[0].split('-');
        var x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
    } else {
        var x = 10000000000000; // = l'an 1000 ...
    }
 
    if (trimX(b) != '') {
        var frDateb = trimX(b).split(' ');
        var frTimeb = frDateb[1].split(':');
        frDateb = frDateb[0].split('-');
        var y = (frDateb[2] + frDateb[1] + frDateb[0] + frTimeb[0] + frTimeb[1] + frTimeb[2]) * 1;                     
    } else {
        var y = 10000000000000;                    
    }
    var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
    return z;
};
 
jQuery.fn.dataTableExt.oSort['date-euro2-desc'] = function(a, b) {
    if (trimX(a) != '') {
        var frDatea = trimX(a).split(' ');
        var frTimea = frDatea[1].split(':');
        var frDatea2 = frDatea[0].split('-');
        var x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;                      
    } else {
        var x = 10000000000000;                    
    }
 
    if (trimX(b) != '') {
        var frDateb = trimX(b).split(' ');
        var frTimeb = frDateb[1].split(':');
        frDateb = frDateb[0].split('-');
        var y = (frDateb[2] + frDateb[1] + frDateb[0] + frTimeb[0] + frTimeb[1] + frTimeb[2]) * 1;                     
    } else {
        var y = 10000000000000;                    
    }                  
    var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));                  
    return z;
};

