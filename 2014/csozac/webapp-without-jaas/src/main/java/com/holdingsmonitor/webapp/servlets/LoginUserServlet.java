package com.holdingsmonitor.webapp.servlets;

import java.io.IOException;
import java.security.Principal;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
@WebServlet("/servlets/LoginUserServlet")
public class LoginUserServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LoginUserServlet.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String errorCode = null;

        try {
            //get username and password
            String username = request.getParameter("j_username");
            String password = request.getParameter("j_password");
            String strForceLogin = request.getParameter("force_login");

            if (username != null && password != null) {
                try {
                    request.login(username, password);
                    Principal principal = request.getUserPrincipal();
                    if (principal == null) {
                        throw new ServletException("getUserPrincipal returned null");
                    }
                    String remoteUser = request.getRemoteUser();
                    if (remoteUser == null) {
                        throw new ServletException("getRemoteUser returned null");
                    }
                    String authType = request.getAuthType();
                    if (authType == null || !authType.equals("LOGIN")) {
                        throw new ServletException("getAuthType returned null or wrong type");
                    }

                    HttpSession httpsession = request.getSession();
                    httpsession.setAttribute("user", username);
                    //redirect to home
                    response.sendRedirect(request.getContextPath() + "/pages/home.jsp");

                } catch (ServletException | IOException ex) {
                    logger.error("No se pudo iniciar Sesion: " + ex.getMessage());
                    errorCode = "ERROR_PASSWORD_NOT_VALID";
                }


            } else {
                HttpSession session = request.getSession();
                if (session != null && session.getAttribute("role") != null) {
                    Object role = session.getAttribute("role");
                    if (role != null) {
                        response.sendRedirect(request.getContextPath() + "/pages/home.jsp");
                    } else {
                        logger.error("Cannot start Session. Missing Data");
                        errorCode = "ERROR_MISSING_DATA";
                    }
                } else {
                    logger.error("Cannot start Session. Missing Data");
                    errorCode = "ERROR_MISSING_DATA";
                }
            }
        } catch (IOException ex) {
            logger.error("Cannot start Session: ", ex);
            errorCode = "ERROR";
        } finally {
            if (errorCode != null) {
                logger.debug("Cannot start Session: " + errorCode);
                request.setAttribute("errorCode", errorCode);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
                dispatcher.forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
