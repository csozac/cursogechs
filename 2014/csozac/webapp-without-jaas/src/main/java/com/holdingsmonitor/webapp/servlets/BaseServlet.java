package com.holdingsmonitor.webapp.servlets;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class BaseServlet {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private static final DecimalFormat NUMBER_FORMAT;

    static {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
        NUMBER_FORMAT = (DecimalFormat) nf;
        NUMBER_FORMAT.applyPattern("###,###,###,##0.0#####");
    }

    public static DecimalFormat getDecimalFormat() {
        return NUMBER_FORMAT;
    }

    public static String getAppEnviroment() {
        return "DEV";
    }
}
